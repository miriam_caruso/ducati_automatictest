#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import *
from RU_005_IscrizioneNewsletter.mailinator_newsletter import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


class RU005 (unittest.TestCase):
    def test_IscrizioneNewsletter(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        second_driver = mailnator
        driver.implicitly_wait(5)
        driver.get(url + "/")

        email = config.get('SEZIONE_EMAIL', 'email')
        print email

        browser.switch_to.window(browser.window_handles[0])

        time.sleep(3)

        try:
           assert "Iscriviti alla newsletter" in driver.find_element_by_css_selector("h3.nl-title").text
        except:
            self.verificationErrors
        assert "" in driver.find_element_by_id("newsletter_email").get_attribute("value")
        try:
            assert "ISCRIVITI ORA" in driver.find_element_by_css_selector("div.field > button.d-button > span.txt").text
        except:
            self.verificationErrors

        driver.find_element_by_id("newsletter_email").click()
        driver.find_element_by_id("newsletter_email").clear()
        driver.find_element_by_id("newsletter_email").send_keys("")
        driver.find_element_by_css_selector("div.field > button.d-button").click()
        try:
            assert u"Questo è un campo obbligatorio." in driver.find_element_by_id("advice-required-entry-newsletter_email").text
        except:
            self.verificationErrors
        try:
            assert u"Questo è un campo obbligatorio."in driver.find_element_by_id("advice-required-entry-nl-cb").text
        except:
            self.verificationErrors
        driver.find_element_by_id("newsletter_email").click()
        driver.find_element_by_id("newsletter_email").clear()
        driver.find_element_by_id("newsletter_email").send_keys("user")
        driver.find_element_by_css_selector("button.d-button.validation-passed > span.txt").click()
        try:
            assert u"Inserire un indirizzo email valido. Per esempio johndoe@domain.com." in driver.find_element_by_id("advice-validate-email-newsletter_email").text
        except:
            self.verificationErrors

        print "I messaggi di warning escono correttamente"

        driver.find_element_by_id("newsletter_email").clear()
        driver.find_element_by_id("newsletter_email").send_keys(email)
        driver.find_element_by_css_selector('svg.icon.icon--check').click()
        driver.find_element_by_css_selector("button.d-button.validation-passed > span.txt").click()
        driver.find_element_by_css_selector("button.d-button.validation-passed")

        time.sleep(4)

        if  driver.find_element_by_css_selector("div.nl--success > h3.nl-title").text == 'Thank you':
            print "Il messaggio di success compare correttamente"
        else:
            print "ERROR: Il messaggio di success non compare"


        second_driver.switch_to.window(second_driver.window_handles[-1])
        time.sleep(5)
        second_driver.refresh()
        second_driver.find_element_by_css_selector("div.all_message-min_text.all_message-min_text-3").click()
        time.sleep(3)
        second_driver.switch_to.frame("msg_body")
        try:
            assert "Ti sei correttamente iscritto alla newsletter." in second_driver.find_element_by_xpath("//h4").text
            print "Email arrivata correttamente"
        finally:
            time.sleep(2)


        try:
            assert u'enabled by Alkemy s.p.a.\nC.f.e.p.i 05619950966 reg. delle imprese di Milano - Cap Soc. 379.961 euro i.v' in second_driver.find_element_by_css_selector("td.footer-intro > p").text
            print ""
        finally:
            time.sleep(1)

        second_driver.find_element_by_xpath("//img[@alt='Ducati']")
        second_driver.find_element_by_xpath("//img[@alt='Ducati su Facebook']")
        second_driver.find_element_by_xpath("//img[@alt='Ducati su Twitter']")
        second_driver.find_element_by_xpath("//img[@alt='Ducati su Instagram']")
        second_driver.find_element_by_xpath("//img[@alt='Ducati su Spotify']")

        print "Verifica sui contenuto dell'email effettuata con successo!"

    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()