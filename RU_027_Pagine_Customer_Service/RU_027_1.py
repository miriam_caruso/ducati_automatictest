#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, shutil, ConfigParser

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_027.py'
print('START ' + nomeTest)


# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU025_1(unittest.TestCase):
    def test_Wishlist(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.get(url + "/")
        print u"Test sulla verifica delle pagine di customer service"

        pathAssoluto = os.getcwd()
        pathFind = pathAssoluto.find('RU_027_Pagine_Customer_Service')
        pathInit = pathAssoluto[:pathFind]
        src = (pathInit + 'RU_004_Login/ConfigFile.properties')
        dst = (pathAssoluto)
        shutil.copy2(src, dst)

        config = ConfigParser.RawConfigParser()
        config.read('ConfigFile.properties')
        print u"I dati dell'utente sono:"

        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print u"Nome: " + nome
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print u"Cognome: " + cognome
        password = config.get('SEZIONE_PASSWORD', 'password')
        print u"Password: " + password
        email = config.get('SEZIONE_EMAIL', 'email')
        print u"Email: " + email
        via = config.get('SEZIONE_DATI_UTENTE', 'via2')
        print "Via: " + via

        driver.find_element_by_link_text("FAQ").click()
        if "FAQ"== driver.title:
           print u"Il redirect alla pagina delle FAQ è corretto"
        else:
            print u"WARNING: Il redirect alla pagina delle FAQ non è corretto"

        driver.find_element_by_link_text("CONDIZIONI DI VENDITA").click()
        if "Terms of sale"==driver.title:
            print u"Il redirect alla pagina TERMINI E CONDIZIONI è corretto"

        driver.find_element_by_link_text("CONTATTI").click()
        if "Contattaci" == driver.title:
            print u'Il redirect al form contattaci è corretto'
        else:
            print u"WARNING: IL REDIRECT alla pagina CONTATTACI non è corretto"

        driver.find_element_by_css_selector("button.d-button").click()
        if u"Questo è un campo obbligatorio." == driver.find_element_by_id("advice-required-entry-comment").text:
            print u" I messaggi di warning compaiono correttamente"
        else:
            print u'Verifica la corretta gestione dei messaggi di warning'

        time.sleep(7)

        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys(nome)
        driver.find_element_by_id("email").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys(email)
        driver.find_element_by_id("telephone").click()
        driver.find_element_by_id("telephone").clear()
        driver.find_element_by_id("telephone").send_keys('000000')
        driver.find_element_by_css_selector("span.switchtext").click()
        driver.find_element_by_css_selector("li[title=\"Suggerimenti\"]").click()
        driver.find_element_by_id("comment").click()
        driver.find_element_by_id("comment").clear()
        driver.find_element_by_id("comment").send_keys("Test")
        driver.find_element_by_css_selector("button.d-button.validation-passed").click()
        time.sleep(7)
        driver.find_element_by_css_selector("#contactForm > div.buttons-set > button").click()
        # if ("Success!" == driver.find_element_by_css_selector("div.d-msg").text and
        #         u"La tua richiesta è stata inviata, riceverai una risposta il prima possibile. Grazie per averci contattato." == driver.find_element_by_css_selector("li > span").text):
        #     print u"Il messaggio di success compare correttamente"
        # else:
        #     print u"WARNING: Verifica il messaggio di success"


        #driver.find_element_by_css_selector("#d-box-close > svg").click()
        driver.find_element_by_link_text("RESI E RIMBORSI").click()
        if "RMA Module" == driver.title:
           print u"Il redirect alla pagina dedicata al RESO è corretto"
        else:
            print u"WARNING: Il redirect alla pagina dedicata al RESO non è corretto"

    def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

    def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

    def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

    def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
        unittest()


