#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, shutil, ConfigParser

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_025.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU025_1(unittest.TestCase):
    def test_Wishlist(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.get(url + "/")
        print u"Test sulla verifica del funzionamento della wishlist"

        pathAssoluto = os.getcwd()
        pathFind = pathAssoluto.find('RU_025_Wishlist')
        pathInit = pathAssoluto[:pathFind]
        src = (pathInit + 'RU_004_Login/ConfigFile.properties')
        dst = (pathAssoluto)
        shutil.copy2(src, dst)

        config = ConfigParser.RawConfigParser()
        config.read('ConfigFile.properties')
        print u"I dati dell'utente sono:"

        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print u"Nome: " +nome
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print u"Cognome: " +cognome
        password = config.get('SEZIONE_PASSWORD', 'password')
        print u"Password: "  +password
        email = config.get('SEZIONE_EMAIL', 'email')
        print u"Email: " +email
        via = config.get('SEZIONE_DATI_UTENTE', 'via2')
        print "Via: " + via




        browser.switch_to.window(browser.window_handles[0])
        browser.find_element_by_css_selector("#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-user.-beopen").click()
        time.sleep(3)
        driver.find_element_by_id("email_menu").clear()
        driver.find_element_by_id("email_menu").send_keys(email)
        driver.find_element_by_id("pass_menu").clear()
        driver.find_element_by_id("pass_menu").send_keys("Utente001")

        driver.find_element_by_css_selector("#send2").click()
        time.sleep(5)

        print "Login effettuata"


        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        print 'click sul tasto hamburger'
        driver.find_element_by_xpath(
            "//div[@id='d-header-main']/div/section/section[3]/div/div/div/div/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        print "redirect alla scheda prodotto"
        time.sleep(4)
        nome=driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
        driver.find_element_by_css_selector("a.d-button.-white.wishlist > span.txt").click()
        time.sleep(4)
        driver.find_element_by_xpath("//div[2]/span/label").click()
        driver.find_element_by_id("wishlist_name").click()
        driver.find_element_by_id("wishlist_name").clear()
        nomeWish= "test wishlist"
        driver.find_element_by_id("wishlist_name").send_keys(nomeWish)
        driver.find_element_by_css_selector("#popup-add-to-wish > span.txt").click()
        time.sleep
        qtyWish=driver.find_element_by_css_selector("span.count").text

        time.sleep(7)
        driver.find_element_by_xpath("//div[2]/ul/li[3]/a/span").click()
        print u"L'icona relativa alla wishlist compare correttamente nell'header"
        if driver.title=="La mia wishlist":
            print u"Il redirect alla pagina della wishlist è corretto"
        else:
            print u"WARNING: verifica il corretto redirect alla wishlist"

        time.sleep(3)
        nomeProdWish=driver.find_element_by_xpath('//h3/a').text
        if nomeProdWish==nome:
            print u"Corretta aggiunta del prodotto alla wishlist"
        else:
            print u"Verifica la corretta aggiunta del prodotto alla wishlist"

        qtyPr = driver.find_element_by_xpath('//td[4]/div/div/input').get_attribute('value')
        if qtyWish== qtyPr:
            print u"La quantità è corretta"
        else:
            print u"WARNING: verifica la quantità"

        print "Creazione nuova wishlist"

        driver.find_element_by_id("wishlist-name").click()
        driver.find_element_by_id("wishlist-name").clear()
        nomeWish2="prova"
        driver.find_element_by_id("wishlist-name").send_keys(nomeWish2)
        driver.find_element_by_id("create-wishlist-button").click()
        time.sleep(4)
        if nomeWish2.upper()==driver.find_element_by_css_selector("a.d-tab.-active").text:
            print u"Wishlist creata con successo"
        else:
            print u"WARNING: verifica la corretta creazione della wishlist"


        driver.find_element_by_css_selector("a.d-tab.-active").click()
        time.sleep(5)
        driver.find_element_by_css_selector("a.d-tab").click()
        driver.find_element_by_css_selector("span.plusqty").click()
        driver.find_element_by_css_selector("span.plusqty").click()
        driver.find_element_by_css_selector("td.-small").click()
        driver.find_element_by_css_selector("div.cart-cell > button.d-button.-slim > span.txt").click()
        time.sleep(5)

        if "Carrello" == driver.title:
            print u'Il prodotto in wishlist è stato aggiunto correttamente al carrello'
        else:
            print u'WARNING: VERIFICA LA CORRETTA AGGIUNTA DEL PRODOTTO AL CARRELLO'

        nomeP=driver.find_element_by_css_selector("tr.last.odd > td.product-cart-info > h2.product-name > a").text
        if nomeP==nomeProdWish:
            print u'Il nome del prodotto è corretto'

        driver.get(url)

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        print 'click sul tasto hamburger'
        time.sleep(3)
        driver.find_element_by_xpath( "//div[@id='d-header-main']/div/section/section[3]/div/div/div/div/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        print "redirect alla scheda prodotto"
        time.sleep(4)

        driver.find_element_by_css_selector("a.d-button.-white.wishlist > span.txt").click()
        time.sleep(4)
        driver.find_element_by_xpath("//div[2]/span/label").click()

        driver.find_element_by_css_selector("#popup-add-to-wish > span.txt").click()
        time.sleep(5)

        #AGGIUNGERE IL PRODOTTO ALLA WISHLIST PROVA


        driver.find_element_by_xpath("//div[2]/ul/li[3]/a/span").click()

        driver.find_element_by_css_selector("a.btn-remove.btn-remove2 > svg").click()
        driver.switch_to_alert().accept()


    def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

    def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

    def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

    def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
        unittest()
