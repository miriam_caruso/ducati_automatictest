#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()

class RU007_2(unittest.TestCase):
    def test_SchedaProdotto(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")

        time.sleep(2)

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        z=0
        urlCategorie = open('urlCategorie')
        option_url = urlCategorie.readlines()
        for urlOption in option_url:
            urlOption.splitlines(1)
            browser.get(url + '/' + urlOption)
            all_option = browser.find_elements_by_tag_name('div')
            #for product in all_option:
            #    if product.get_attribute('class') == 'd-card-imgtext -products':
            #        nameP = product.text
            #        print u"Nella sezione Ultimi arrivi è presente il prodotto:" + nameP
            driver.find_element_by_css_selector('a.picturewrap.product-image > picture.picture').click()
            titoloPag= driver.title
            print "Sei nella pagina del prodotto: " +titoloPag
            nameP= driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
            print u"Il nome del prodotto è: " +nameP
            codP=driver.find_element_by_css_selector('div.extra-info._hide-small-mobile > div.left > span.product-sku').text
            print u"Il codice del prodotto è: " +codP
            disp=driver.find_element_by_css_selector('span.value').text
            print u"Il prodotto è: " +disp
            price=driver.find_element_by_css_selector('span.price > span.price').text
            print u"Il prezzo del prodotto è: " + price
            time.sleep(5)
            if driver.find_elements_by_xpath('//div[4]/div[2]/div[2]/button/span'):
                print u"Il pulsante AGGIUNGI AL CARRELLO è presente"
            else:
                print "WARNING: Verifica se è presente il pulsante aggiungi al carrello"

            if driver.find_element_by_css_selector('a.d-button.-white.wishlist'):
                print u"Il pulsante WISHLIST è presente"
            else:
                print "WARNING: Verifica se è presente il pulsante WISHLIST"
            time.sleep(4)

            if (driver.find_element_by_css_selector('div.body > span').text=='Free\nshipping' and
                driver.find_element_by_css_selector('div.box.payments > div.body > span').text=='Secure\npayments'and
                driver.find_element_by_css_selector('div.box.returns > div.body > span').text=='Free\nreturns'):
                print "Spedizione gratuita, pagamenti sicuri, resi gratuiti presente"
            else:
                print u"WARNING: Verifica la presenza delle label spedizione gratuita, pagamenti sicuri, resi gratuiti"

            if (driver.find_element_by_css_selector('svg.icon.icon--social-fb') and
                driver.find_element_by_css_selector('svg.icon.icon--social-twitter') and
                driver.find_element_by_css_selector('svg.icon.icon--social-googleplus')):
                print "Icone social presenti"
            else:
                print "Manca qualche icona social"

            driver.find_element_by_xpath("//picture/a/img").click()
            driver.find_element_by_css_selector(
                "div.fancybox-slide.fancybox-slide--current.fancybox-slide--image").click()
            driver.find_element_by_css_selector('button.fancybox-button.fancybox-button--close').click()

            browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")

            time.sleep(5)

            if driver.find_element_by_css_selector('div.block-title.title.active').text=='DESCRIZIONE':
                driver.find_element_by_css_selector('div.block-content.content.open').text
                print "Il campo Descrizione è presente"
            else:
                print "WARNING: Verifica che la descrizione sia presente"
            z=z+1
            if (z==8):
                break



    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()






