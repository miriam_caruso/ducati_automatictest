#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()

nomeTest = 'RU_001_3.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper


class RU001_2(unittest.TestCase):
    def test_fooder(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        time.sleep(9)
        i=0
        j=33
        z=0
        #browser.find_element_by_css_selector("#nw_close>svg").click()
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        browser.find_element_by_css_selector("span.label").click()

        try:
           self.assertEqual("Scegli il tuo paese o regione",
                         driver.find_element_by_css_selector("div.d-title-full > div.title").text)
        except AssertionError as e:
           self.verificationErrors.append(str(e))


        all_option= browser.find_elements_by_tag_name('a')
        for option in all_option:
            if option.get_attribute('class')== "switch-link":
                name=option.get_attribute('innerHTML')
                print name

                countryOnSite = open('country.txt')
                CoS=countryOnSite.readlines()
                for nameCountry in CoS:
                   nameCountry.splitlines(1)
                   n=name.encode().upper()
                   if n+u'\n'== nameCountry:
                        print ('la country '+ name + u' è presente')
                        i=i+1
                        break
                   if n==nameCountry:
                        print ('la country ' + name + u' è presente')
                        i=i+1
            if option.get_attribute('class') == "switch-link active":
                  nameselected = option.get_attribute('innerHTML')
                  if nameselected==u'Italia':
                      print (u'la country  selezionato è '+ nameselected)
                      i=i+1
            if option.get_attribute('data-finalrul'):
               urlC=option.get_attribute('data-finalrul')

               urlCountry = open('URLcountry.txt')
               option_url = urlCountry.readlines()
               for urlOption in option_url:
                urlOption.splitlines(1)
                if urlOption == urlC.encode()+'\n':
                    print ' indirizzo ' + urlC + u' è corretto'
                    z=z+1



        if i==j:
            print('tutti i country sono presenti')
        else:
            print('verifica la mancanza di qualche country')

        if z==j:
            print('tutte le URL sono presenti')
        else:
            print('verifica la mancanza di qualche URL')

        urlCountry = open('URLcountry.txt')
        option_url = urlCountry.readlines()
        for urlOption in option_url:
                urlOption.splitlines(1)
                driver.get(urlOption)
                if driver.title== "Home page":
                    print 'redirect corretto'

    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()




