#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, shutil, ConfigParser

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_028_1.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU023_1(unittest.TestCase):
    def test_Reso(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.get(url + "/")

        pathAssoluto = os.getcwd()
        pathFind = pathAssoluto.find('RU_028_Modulo_di_reso')
        pathInit = pathAssoluto[:pathFind]
        src = (pathInit + 'RU_004_Login/ConfigFile.properties')
        dst = (pathAssoluto)
        shutil.copy2(src, dst)

        config = ConfigParser.RawConfigParser()
        config.read('ConfigFile.properties')
        print u"I dati dell'utente sono:"

        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print u"Nome: " + nome
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print u"Cognome: " + cognome
        password = config.get('SEZIONE_PASSWORD', 'password')
        print u"Password: " + password
        email = config.get('SEZIONE_EMAIL', 'email')
        print u"Email: " + email
        via = config.get('SEZIONE_DATI_UTENTE', 'via2')
        print "Via: " + via

        browser.switch_to.window(browser.window_handles[0])
        browser.find_element_by_css_selector(
            "#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-user.-beopen").click()
        time.sleep(3)
        driver.find_element_by_id("email_menu").clear()
        driver.find_element_by_id("email_menu").send_keys(email)
        driver.find_element_by_id("pass_menu").clear()
        driver.find_element_by_id("pass_menu").send_keys(password)

        driver.find_element_by_css_selector("#send2").click()
        time.sleep(5)


        driver.find_element_by_id("pass").send_keys('Utente001')

        driver.find_element_by_css_selector("div.buttons-set > #send2").click()
        print 'accesso effettuato con successo'

