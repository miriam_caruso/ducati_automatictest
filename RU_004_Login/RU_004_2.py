#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()


nomeTest = 'RU_004_2.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU004_2(unittest.TestCase):
    def test_registrazione_con_credenziali(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None

        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")



        browser.find_element_by_css_selector("li.link.top-user.-mobilewasteful>a>svg").click()
        time.sleep(2)
        driver.switch_to_active_element()
        driver.find_element_by_css_selector( "div.level-two.loginmenuwrapper.dx-wrapper>div.scroll>div.columns-wrap").click()
        driver.find_element_by_css_selector("a.d-button.-gray > span").click()
        time.sleep(5)
        try:
            self.assertEqual("Crea un nuovo account cliente", driver.title)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'il title Crea un acount è presente'
        try:
            self.assertEqual("Completa le informazioni sottostanti per creare un account.",
                             driver.find_element_by_css_selector("p.form-instructions").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'il title: completa informazioni per creare un account è presente'
        try:
            self.assertEqual("", driver.find_element_by_id("firstname").get_attribute("value"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'la casella Nome è presente'
        try:
            self.assertEqual("", driver.find_element_by_id("lastname").get_attribute("value"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
            print 'la casella Cognome è presente'
        try:
            self.assertEqual("", driver.find_element_by_id("email_address").get_attribute("value"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print ' la casella Indirizzo email è presente'
        try:
            self.assertEqual("", driver.find_element_by_id("password").get_attribute("value"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'la casella password è presente'
        try:
            self.assertEqual("", driver.find_element_by_id("confirmation").get_attribute("value"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print ' la casella Conferma password è presente'
        try:
            self.assertEqual("off", driver.find_element_by_id("is_subscribed").get_attribute("value"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print ' la casella check è presente'
        try:
            self.assertEqual("Iscriviti alla newsletter", driver.find_element_by_xpath(
                "//form[@id='form-validate']/div/ul/li[4]/div/div/span/label").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'il testo: iscriviti alla newsletter, è presente'
        try:
            self.assertRegexpMatches(driver.find_element_by_xpath("//form[@id='form-validate']/div[2]/p").text,
                                     r"^[\s\S]* Campi obbligatori$")
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'il testo: *campi obbligatori, è presente'

        try:
            self.assertEqual("Indietro", driver.find_element_by_xpath(
                "(//a[contains(@href, 'https://ducati.alkemy-stage.com/it/it/customer/account/login/')])[3]").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'il tasto indietro è presente'
        try:
            self.assertEqual("Registrati",
                             driver.find_element_by_xpath("//form[@id='form-validate']/div[2]/button/span").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'il tasto Registrati è presente'

        #verifica allert campi obbligatori
        driver.find_element_by_css_selector("button.d-button.-gray.right").click()
        try:
            self.assertEqual(u"Questo è un campo obbligatorio.",
                             driver.find_element_by_id("advice-required-entry-firstname").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'campo obbligatorio box Nome presente'
        try:
            self.assertEqual(u"Questo è un campo obbligatorio.",
                             driver.find_element_by_id("advice-required-entry-lastname").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'campo obbligatorio box Cognome presente'
        try:
            self.assertEqual(u"Questo è un campo obbligatorio.",
                             driver.find_element_by_id("advice-required-entry-email_address").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'campo obbligatorio box email presente'

        try:
            self.assertEqual(u"Questo è un campo obbligatorio.",
                             driver.find_element_by_id("advice-required-entry-password").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'campo obbligatorio box password presente'
        try:
            self.assertEqual(u"Questo è un campo obbligatorio.",
                                 driver.find_element_by_id("advice-required-entry-confirmation").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print 'campo obbligatorio box conferma psw presente'
















        def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

        def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

        def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

        def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

        if __name__ == "__main__":
            unittest()