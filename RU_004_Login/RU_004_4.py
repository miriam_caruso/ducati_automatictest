#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, ConfigParser
reload(sys)
sys.getdefaultencoding()

nomeTest = 'RU_00_4.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper




class RU004_4(unittest.TestCase):
    def test_VerificaDatiAccount(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        config = ConfigParser.RawConfigParser()
        config.read('ConfigFile.properties')

        browser.find_element_by_css_selector("#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-user.-beopen").click()
        time.sleep(3)
        config.set('SEZIONE_DATI_UTENTE', 'nome', 'Utente')
        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print nome
        config.set('SEZIONE_DATI_UTENTE', 'cognome', 'Test')
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print cognome
        config.set('SEZIONE_PASSWORD', 'password', 'UtenteTest001')
        password = config.get('SEZIONE_PASSWORD', 'password')
        print password
        email = config.get('SEZIONE_EMAIL', 'email')
        print email

        with open('ConfigFile.properties', 'w') as configfile:
            config.write(configfile)








        driver.find_element_by_id("email_menu").clear()
        driver.find_element_by_id("email_menu").send_keys(email)
        driver.find_element_by_id("pass_menu").clear()
        driver.find_element_by_id("pass_menu").send_keys(password)

        driver.find_element_by_css_selector("#send2").click()
        time.sleep(5)


        if driver.title =='Il mio account':

            print 'redirect eseguito con successo'












        def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

        def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

        def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

        def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

        if __name__ == "__main__":
            unittest()