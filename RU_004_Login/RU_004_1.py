#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()

nomeTest = 'RU_004_1.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU004_1(unittest.TestCase):
    def test_login(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        driver.find_element_by_css_selector("li.link.top-user.-beopen > a > svg").click()
        time.sleep(2)
        driver.switch_to_active_element()
        driver.find_element_by_css_selector( "div.level-two.loginmenuwrapper.dx-wrapper>div.scroll>div.columns-wrap").click()

        try:
            assert "Nuovo utente" in driver.find_element_by_css_selector("p.title-miniform").text
            print "Nuovo Utente ok"
        finally:
            time.sleep(2)


        try:
            assert "By creating an account with our store you will be able to move quickly to orders, place orders with multiple shipping addresses, view and track your orders in your account and much more!" in driver.find_element_by_xpath("//form[@id='login-form']/div/div/div/p[2]").text
            print "testo ok"
        finally:
            time.sleep(2)


        try:
            assert "REGISTRATI ORA" in  driver.find_element_by_css_selector("a.d-button.-gray > span.txt").text
            print "Registrati ora ok"
        finally:
            time.sleep(2)


        try:
            assert "Accedi" in driver.find_element_by_xpath("//form[@id='login-form']/div/div[2]/div/p").text
            print "Accedi ok"
        finally:
            time.sleep(2)


        try:
           assert "Hai dimenticato la password" in driver.find_element_by_css_selector("li.forgot-password > a").text
           print "Hai dimenticato la password ok"
        finally:
           time.sleep(2)


        try:
            assert"ACCEDI" in  driver.find_element_by_css_selector("#send2").text
            print "Hai dimenticato la password ok"
        finally:
            time.sleep(2)











        def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

        def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

        def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

        def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

    if __name__ == "__main__":
        unittest()
