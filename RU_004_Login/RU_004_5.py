#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()



nomeTest = 'RU_004_5.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper



class RU004_5(unittest.TestCase):
    def test_CredenzialiErrateAccount(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        browser.find_element_by_css_selector("#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-user.-beopen").click()
        time.sleep(3)
        driver.find_element_by_id("email_menu").clear()
        driver.find_element_by_id("email_menu").send_keys("MarcoRossi@gmail.com")
        driver.find_element_by_id("pass_menu").clear()
        driver.find_element_by_id("pass_menu").send_keys("12345Psw")

        driver.find_element_by_css_selector("#send2").click()

        if (driver.find_element_by_css_selector("div.d-msg").text == "Error" and driver.find_element_by_css_selector(
                "li > span").text == "Login o password non validi"):
            print 'testo ERROR è presente'
            print 'testo Login o psw non validi presenti'
        else:
            print 'WARNING:verifica la mancanza di qualche sezione del popup'
        driver.find_element_by_css_selector("path.d_close").click()


        if (driver.find_element_by_css_selector("h4").text=="Accedi" and driver.find_element_by_css_selector("#email").get_attribute("value")=="" and
            driver.find_element_by_css_selector("a.f-pwd").text== u"Hai dimenticato la password?" and
            driver.find_element_by_css_selector("div.buttons-set > #send2").text=="ACCEDI" and
            driver.find_element_by_xpath("//form[@id='login-form']/div[4]/div[2]/ul/li/a/span").text=="LOGIN WITH FACEBOOK" and
            driver.find_element_by_css_selector("div.block-new-user > div.std > h4").text=="Nuovo utente" and
            driver.find_element_by_css_selector("div.std > p").text== "Creando un account con il nostro negozio sarai in grado di muoverti velocemente verso gli ordini, effettuare ordini con indirizzi di spedizione multipli, visualizzare e tracciare i tuoi ordini nel tuo account"and
            driver.find_element_by_css_selector("div.block-new.col-xs-12.col-md.col-md-offset-1.col-lg.col-lg-offset-1 > div.buttons-set > button.d-button.-gray > span.txt").text =="REGISTRATI ORA"):

               print 'il testo Accedi è presente'
               print 'il box Password* è presente'
               print 'il tasto Accedi è presente'
               print 'il tasto Login with Facebook è presente'
               print 'il testo Nuovo utente è presente'
               print 'il testo Crea un account.. è presente'

               print 'il tasto Registrati ora è presente'
        else:
               print 'WARNING:verifica la mancanza di qualche sezione'

        time.sleep(3)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("MarcoRossi@gmail.com")
        driver.find_element_by_id("pass").clear()
        driver.find_element_by_id("pass").send_keys("12345Psw")
        driver.find_element_by_css_selector("div.buttons-set > #send2").click()

        if (driver.find_element_by_css_selector("div.d-msg").text=="Error" and driver.find_element_by_css_selector("li > span").text=="Login o password non validi"):
              print 'testo ERROR è presente'
              print 'testo Login o psw non validi presenti'
        else:
              print 'WARNING:verifica la mancanza di qualche sezione del popup'
        driver.find_element_by_css_selector("path.d_close").click()



        def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

        def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

        def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

        def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

        if __name__ == "__main__":
            unittest()
