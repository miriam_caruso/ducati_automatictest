#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import *
from RU_004_Login.mailinator import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


nomeTest = 'RU_004_3.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper


class RU004_3(unittest.TestCase):
    def test_registrazione_account(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        second_driver = mailnator
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")



        config.set('SEZIONE_DATI_UTENTE', 'nome', 'Utente')
        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print nome
        config.set('SEZIONE_DATI_UTENTE', 'cognome', 'Test')
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print cognome
        config.set('SEZIONE_PASSWORD', 'password', 'UtenteTest001')
        password = config.get('SEZIONE_PASSWORD', 'password')
        print password
        email = config.get('SEZIONE_EMAIL', 'email')
        print email

        with open('ConfigFile.properties', 'w') as configfile:
            config.write(configfile)


        browser.switch_to.window(browser.window_handles[0])
        browser.find_element_by_css_selector("li.link.top-user.-mobilewasteful>a>svg").click()
        time.sleep(2)
        driver.switch_to_active_element()
        driver.find_element_by_css_selector(
            "div.level-two.loginmenuwrapper.dx-wrapper>div.scroll>div.columns-wrap").click()
        driver.find_element_by_css_selector("a.d-button.-gray > span").click()
        time.sleep(5)


        driver.find_element_by_id("firstname").send_keys(nome)
        print"Nome utente:" + nome

        driver.find_element_by_id("lastname").clear()
        driver.find_element_by_id("lastname").send_keys(cognome)
        print "Cognome Utente: " + cognome

        driver.find_element_by_id("email_address").clear()

        driver.find_element_by_id("email_address").send_keys(email)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys(password)
        print password

        driver.find_element_by_id("confirmation").clear()
        driver.find_element_by_id("confirmation").send_keys(password)

        driver.find_element_by_css_selector("#form-validate > div.buttons-set > button > span").click() #click sul bottone registrati
        driver.switch_to_active_element()
        try:
            self.assertEqual("Success!", driver.find_element_by_css_selector("div.d-msg").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(
                "Conferma account richiesto. Per favore, controlla la tua e-mail per il link di conferma.. Per rinviare l'email di conferma clicca qui.",
                driver.find_element_by_css_selector("li > span").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("#d-box-close > svg").click()




        second_driver.switch_to.window(second_driver.window_handles[-1])
        time.sleep(3)
        second_driver.refresh()
        time.sleep(5)
        second_driver.find_element_by_css_selector("div.all_message-min_text.all_message-min_text-3").click()
        time.sleep(5)


        second_driver.switch_to.frame("msg_body")


        second_driver.find_element_by_xpath("//img[@alt='Ducati']")



        try:
            assert nome + " " +  cognome + "," in second_driver.find_element_by_xpath("//h1").text
            print "ok"
        finally:
            time.sleep(3)

        try:
            assert "La tua email" + " " + email  + " deve essere confermata prima di essere utilizzata per l'accesso al nostro negozio." in second_driver.find_element_by_css_selector("td.action-content > p").text
            print "ok1"

        finally:
            time.sleep(3)

        try:
           assert u"Utilizza i seguenti parametri quando devi effettuare l'accesso::\nEmail: " + email + "\nPassword: " + password in second_driver.find_element_by_css_selector(  "p.highlighted-text").text
           print "ok2"

        finally:
           time.sleep(3)
        try:
           assert u"Clicca qui per confermare l'account e fare subito il login (il link sarà valido una sola volta):" in second_driver.find_element_by_xpath("//p[3]").text
           print "ok3"

        finally:
           time.sleep(3)

        try:
            assert "CONFERMA ACCOUNT" in second_driver.find_element_by_xpath("//span").text
            print "ok4"

        finally:
            time.sleep(3)

        try:
            assert u"Se hai qualche domanda contattaci al nostro indirizzo email ducati@alkemy-stage.com ."  in second_driver.find_element_by_xpath("//p[4]").text
            print "ok5"
        finally:
            time.sleep(3)








        url_conf = second_driver.find_element_by_css_selector("table.action-button > tbody > tr > td > a").get_attribute('href')
        print "L'email di conferma è arrivata correttamente"

        browser.switch_to.window(browser.window_handles[0])
        browser.get(url_conf)

        try:
             assert "Success!" in driver.find_element_by_css_selector("div.d-msg").text
             print "success ok"
        finally:
            time.sleep(3)

        try:
           assert "Grazie per esserti registrato con Ducati." in driver.find_element_by_css_selector("li.success-msg > ul > li > span").text
           print "ok6"
        finally:
            time.sleep(3)

        driver.find_element_by_css_selector("#d-box-close > svg").click()
        driver.find_element_by_css_selector("li.link.top-user.-mobilewasteful.-beopen > a > svg").click()

        try:
            assert "Hi Utente", driver.find_element_by_css_selector("p.title-miniform").text
            print "hi presente"
        finally:
            time.sleep(3)












def is_element_present(self, how, what):
    try:
        browser.find_element(by=how, value=what)
    except NoSuchElementException as e:
        return False
    return True


def is_alert_present(self):
    try:
        browser.switch_to_alert()
    except NoAlertPresentException as e:
        return False
    return True


def close_alert_and_get_its_text(self):
    try:
        alert = browser.switch_to_alert()
        alert_text = alert.text
        if self.accept_next_alert:
            alert.accept()
        else:
            alert.dismiss()
        return alert_text
    finally:
        self.accept_next_alert = True


def tearDown(self):
    browser.quit()
    self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()