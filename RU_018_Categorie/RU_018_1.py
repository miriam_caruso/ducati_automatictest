#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display


from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_018_1.py'
print('START '+nomeTest)

# SET for Bamboo  (QUANDO FACCIO LA COMMIT VA TOLTO IL COMMENTO)
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


class RU0018_1(unittest.TestCase):
    def test_Presenza_delle_sezioni(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.delete_all_cookies()
        print ("eliminati tutti i cookies")
        driver.get(url + "/")

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        print 'click sul tasto hamburger'


        try:
            assert "ABBIGLIAMENTO MOTO" in driver.find_element_by_css_selector("div.title > a").text
            print 'la sub-categoria Abbigliamento Moto è presente'
        finally:
            time.sleep(2)

        try:
            assert "ABBIGLIAMENTO CASUAL" in driver.find_element_by_xpath("//div/div/div[2]/div/a").text
            print 'la sub-categoria Abbigliamento Casual è presente'
        finally:
            time.sleep(2)
        try:
            assert  "ACCESSORI" in driver.find_element_by_xpath("//div/div[3]/div/a").text
            print 'la sub-categoria Accessori è presente'
        finally:
            time.sleep(2)
        try:
            assert "SCRAMBLER" in driver.find_element_by_xpath("//div/div/div/div[4]/div/a").text
            print 'la sub-categoria Scrambler è presente'
        finally:
            time.sleep(2)

        i = 1
        j = 27
        all_option = driver.find_elements_by_tag_name('a')
        for categories in all_option:
            if categories.get_attribute('title'):
                categorieOnSite = open('categorie.txt')
                CoS = categorieOnSite.readlines()
                for categorie in CoS:
                    categorie.splitlines(1)
                    cat = categories.get_attribute('title')
                    if i < 27:
                        if cat.encode() + '\n' == categorie:
                            print "La categoria " + cat + u' è presente'
                            i = i + 1
                            break
                    if i > 27:
                        break

        if (i == j):
            print "Tutte le categorie sono presenti"
        else:
            print "WARNING: manca qualche categoria"

        z = 0

        urlCategorie = open('urlCategorie')
        option_url = urlCategorie.readlines()
        for urlOption in option_url:
            urlOption.splitlines(1)
            browser.get(url + '/' + urlOption)
            # time.sleep(3)
            categorieOnSite = open('categorie.txt')
            CoS = categorieOnSite.readlines()
            for categorie in CoS:
                categorie.splitlines(1)
                verCat = categorie.replace('\n', '')
                if driver.title == verCat.decode() + " - Abbigliamento Moto" \
                        or driver.title == verCat.decode() + " - Abbigliamento Casual" \
                        or driver.title == verCat.decode() + " - Accessori":
                    print "Il redirect alla pagina della categoria " + categorie + u' è corretto'
                    z = z + 1
                    break
        if (z == j):
            print "Il redirect è corretto per tutte le categorie"
        else:
            print "WARNING: Il redirect per qualche categoria è sbagliato"




        def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

        def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

        def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

        def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

        if __name__ == "__main__":
            unittest()
