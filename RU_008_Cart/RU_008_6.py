#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, shutil, ConfigParser

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_008_6.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)




class RU008_6(unittest.TestCase):
    def test_Cart_Copoun(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")


        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(8)
        print 'click sul tasto hamburger'
        driver.find_element_by_xpath( "//div[@id='d-header-main']/div/section/section[3]/div/div/div/div/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        print "redirect alla scheda prodotto"
        time.sleep(4)

        titoloPag = driver.title
        print "Sei nella pagina del prodotto: " + titoloPag
        nameP = driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
        print u"Il nome del prodotto è: " + nameP
        codP = driver.find_element_by_css_selector(
                'div.extra-info._hide-small-mobile > div.left > span.product-sku').text
        print u"Il codice del prodotto è: " + codP
        disp = driver.find_element_by_css_selector('p.in-stock').text
        print u"Il prodotto è: " + disp
        price = driver.find_element_by_css_selector('span.price > span.price').text
        print u"Il prezzo del prodotto è: " + price
        time.sleep(7)

        driver.find_element_by_css_selector(
            '#product_addtocart_form > div.add-to-cart-wrapper > div.add-to-box > div > div.add-to-cart-buttons > button').click()
        time.sleep(1)
        driver.find_element_by_css_selector(
            'body > div.global-wrapper.page-wrapper > div > div.header-wrapper > div.d-header-main.-shortcutnavopen.-righttopen > div > section > section:nth-child(3) > div.level-two.cartmenuwrapper.dx-wrapper > div')

        time.sleep(8)
        driver.find_element_by_css_selector("span.icon.no-empty").click()
        time.sleep(3)
        print "Aggiunta del prodotto al carrello"
        if (driver.find_element_by_css_selector("div.product-name > a > span").text == nameP and
                    driver.find_element_by_css_selector("span.price").text == price):
            print "Il nome e prezzo del prodotto presenti nel mini cart è corretto"
        else:
            print "WARNING: Il nome e il prezo del prodotto non sono corretti"
        if (driver.find_element_by_css_selector("div.qty-wrapper > span").text == u"Qtà" and
                    driver.find_element_by_css_selector('span.qty').text == '1'):
            print u"La quantità è presente"

        try:
            assert "Subtotale carrello:" in driver.find_element_by_css_selector("div.subtotal > span.label").text
            print "Subtotale presente nel mini cart"
        finally:
            time.sleep(3)
        if driver.find_element_by_css_selector("div.subtotal > span.price").text == price:
            print "Subtotale ordine corretto nel mini cart"

        try:
            assert "CONTINUA L'ACQUISTO" in driver.find_element_by_css_selector(
                "a.cart-link.d-button.-white.close-cart").text
            print u"Il pulsante CONTINUA L'ACQUISTO è presente"
        finally:
            time.sleep(2)
        try:
            assert "PROCEDI" in driver.find_element_by_css_selector("a.d-button.btn-proceed-checkout.btn-checkout").text
            print u"Il pulsante PROCEDI è presente"
        finally:
            time.sleep(2)



        driver.find_element_by_css_selector("a.d-button.btn-proceed-checkout.btn-checkout").click()
        if (nameP == driver.find_element_by_css_selector("h2.product-name > a").text and
                    driver.find_element_by_css_selector("span.cart-price > span.price").text == price):
            print u"Il prodotto nel cart è corretto"
        else:
            print u"WARNING:Il prodotto nel cart NON è corretto"

        if '1' == driver.find_element_by_xpath("//td[4]/div/input").get_attribute("value"):
            print u"La quantità nel cart è corretta"
        else:
            print u"WARNING: la quantità nel cart NON è corretta"



        totqty=driver.find_element_by_css_selector("td.product-cart-total > span.cart-price > span.price").text
        tqty = totqty.replace(',', '.')
        TQ= tqty[:-2].encode()


        if float(TQ) >=100:
            if driver.find_element_by_css_selector("td.product-cart-total > span.cart-price > span.price").text== totqty:
                print u"Il totale per ordini superiori a 100 euro è corretto"
                print u'La spedizione è gratuita'

        else:
            spedizione= driver.find_element_by_css_selector('#shopping-cart-totals-table > tbody > tr:nth-child(2) > td:nth-child(2)').text
            sped = spedizione.replace(',', '.')
            S = sped[:-2].encode()
            sum= round(float(S), 1)+float(TQ)
            totComplessivo= driver.find_element_by_css_selector("strong > span.price").text
            totC = totComplessivo.replace(',', '.')
            TC = totC[:-2].encode()
            if round(float(TC),1)==sum:
                print u"Per ordini inferiori a 100 euro il costo della spedizione è 6,50 euro "
                print u'Il totale complessivo è corretto'

        driver.find_element_by_id("coupon_code").click()
        driver.find_element_by_id("coupon_code").clear()
        driver.find_element_by_id("coupon_code").send_keys("test1234")
        driver.find_element_by_css_selector("div.discount-form > div.input-box > button.button > span > span").click()

        try:
            assert "Sconto (TEST COUPON)" in driver.find_element_by_xpath(
                "//table[@id='shopping-cart-totals-table']/tbody/tr[2]/td").text
            print "Sconto applicato"
        finally:
            time.sleep(2)

        sconto= float(TQ)*0.10

        sale=driver.find_element_by_xpath("//tr[2]/td[2]/span").text
        sal = sale.replace(',', '.')
        sales = sal[:-2].encode()
        sl=sales[1:]

        if  round(float(sconto) ,1)==round (float(sl) ,1):
            print "Lo sconto del 10% è stato calcolato correttamente"

        totsales= driver.find_element_by_css_selector("strong > span.price").text
        totsal = totsales.replace(',', '.')
        totS = totsal[:-2].encode()

        totalDiscount=float(TQ) - float(sl)

        if totalDiscount <= 100:
            tot3=totalDiscount+6.50
            if float(tot3) ==float(totS):
               print "Totale con sconto calcolato correttamente"
        else:
            if float(totalDiscount) == float(totS):
                print "Totale con sconto calcolato correttamente"



    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()