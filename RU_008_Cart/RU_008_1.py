#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


class RU003_1(unittest.TestCase):
    def test_HeaderFooter(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")


        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(8)
        print 'click sul tasto hamburger'
        driver.find_element_by_xpath( "//div[@id='d-header-main']/div/section/section[3]/div/div/div/div/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        print "redirect alla scheda prodotto"
        time.sleep(4)

        titoloPag = driver.title
        print "Sei nella pagina del prodotto: " + titoloPag
        nameP = driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
        print u"Il nome del prodotto è: " + nameP
        codP = driver.find_element_by_css_selector(
                'div.extra-info._hide-small-mobile > div.left > span.product-sku').text
        print u"Il codice del prodotto è: " + codP
        disp = driver.find_element_by_css_selector('p.in-stock').text
        print u"Il prodotto è: " + disp
        price = driver.find_element_by_css_selector('span.price > span.price').text
        print u"Il prezzo del prodotto è: " + price
        time.sleep(7)

        driver.find_element_by_css_selector(
            '#product_addtocart_form > div.add-to-cart-wrapper > div.add-to-box > div > div.add-to-cart-buttons > button').click()
        time.sleep(1)
        driver.find_element_by_css_selector(
            'body > div.global-wrapper.page-wrapper > div > div.header-wrapper > div.d-header-main.-shortcutnavopen.-righttopen > div > section > section:nth-child(3) > div.level-two.cartmenuwrapper.dx-wrapper > div')

        time.sleep(8)
        driver.find_element_by_css_selector("span.icon.no-empty").click()
        time.sleep(3)
        print "Aggiunta del prodotto al carrello"
        if (driver.find_element_by_css_selector("div.product-name > a > span").text == nameP and
                    driver.find_element_by_css_selector("span.price").text == price):
            print "Il nome e prezzo del prodotto presenti nel mini cart è corretto"
        else:
            print "WARNING: Il nome e il prezo del prodotto non sono corretti"
        if (driver.find_element_by_css_selector("div.qty-wrapper > span").text == u"Qtà" and
                    driver.find_element_by_css_selector('span.qty').text == '1'):
            print u"La quantità è presente"

        try:
            assert "Subtotale carrello:" in driver.find_element_by_css_selector("div.subtotal > span.label").text
            print "Subtotale presente nel mini cart"
        finally:
            time.sleep(3)
        if driver.find_element_by_css_selector("div.subtotal > span.price").text == price:
            print "Subtotale ordine corretto nel mini cart"

        try:
            assert "CONTINUA L'ACQUISTO" in driver.find_element_by_css_selector(
                "a.cart-link.d-button.-white.close-cart").text
            print u"Il pulsante CONTINUA L'ACQUISTO è presente"
        finally:
            time.sleep(2)
        try:
            assert "PROCEDI" in driver.find_element_by_css_selector("a.d-button.btn-proceed-checkout.btn-checkout").text
            print u"Il pulsante PROCEDI è presente"
        finally:
            time.sleep(2)

        print "Rimozione prodotto dal mini cart"


        driver.find_element_by_css_selector("a.remove > svg").click()
        time.sleep(2)
        driver.switch_to_alert().accept()

        time.sleep(4)

        if u"L'oggetto è stato rimosso correttamente."==driver.find_element_by_id("minicart-success-message").text:
            print u'Il prodotto è stato rimosso correttamente dal minicart'
        else:
            print u'WARNING: Verifica il corretto funzionamento della rimozione dei prodotti dal mini cart'


        driver.get(url)
        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(8)
        print 'click sul tasto hamburger'
        driver.find_element_by_xpath( "//div[@id='d-header-main']/div/section/section[3]/div/div/div/div/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        print "redirect alla scheda prodotto"
        time.sleep(4)

        titoloPag = driver.title
        print "Sei nella pagina del prodotto: " + titoloPag
        nameP = driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
        print u"Il nome del prodotto è: " + nameP
        codP = driver.find_element_by_css_selector(
            'div.extra-info._hide-small-mobile > div.left > span.product-sku').text
        print u"Il codice del prodotto è: " + codP
        disp = driver.find_element_by_css_selector('p.in-stock').text
        print u"Il prodotto è: " + disp
        price = driver.find_element_by_css_selector('span.price > span.price').text
        print u"Il prezzo del prodotto è: " + price
        time.sleep(7)

        driver.find_element_by_css_selector(
            '#product_addtocart_form > div.add-to-cart-wrapper > div.add-to-box > div > div.add-to-cart-buttons > button').click()
        time.sleep(1)
        print "Aggiunta prodotti al carrello"
        driver.find_element_by_css_selector(
            'body > div.global-wrapper.page-wrapper > div > div.header-wrapper > div.d-header-main.-shortcutnavopen.-righttopen > div > section > section:nth-child(3) > div.level-two.cartmenuwrapper.dx-wrapper > div')

        time.sleep(8)
        driver.find_element_by_css_selector("span.icon.no-empty").click()
        time.sleep(3)
        driver.find_element_by_css_selector("a.d-button.btn-proceed-checkout.btn-checkout").click()
        if (nameP == driver.find_element_by_css_selector("h2.product-name > a").text and
                    driver.find_element_by_css_selector("span.cart-price > span.price").text == price):
            print u"Il prodotto nel cart è corretto"
        else:
            print u"WARNING:Il prodotto nel cart NON è corretto"

        if '1' == driver.find_element_by_xpath("//td[4]/div/input").get_attribute("value"):
            print u"La quantità nel cart è corretta"
        else:
            print u"WARNING: la quantità nel cart NON è corretta"

        print u"Incremento quantità nel cart"

        driver.find_element_by_css_selector("span.plusqty").click()
        driver.find_element_by_css_selector("span.plusqty").click()
        driver.find_element_by_css_selector("span.plusqty").click()

        print u'Decremento quantità nel cart'
        driver.find_element_by_css_selector("span.minqty").click()
        if "3" == driver.find_element_by_xpath("//td[4]/div/input").get_attribute("value"):
            print u"L'incremento e il decremento della quantità funziona correttamente"
        else:
            print u"WARNING:L'incremento e il decremento della quantità NON funziona correttamente"
        p= price.replace(',' , '.')
        pr= p[:-2].encode()

        tot = 3 * float(pr)

        print u"Verifica in corso sull'aggiornamento del prezzo"

        totqty=driver.find_element_by_css_selector("td.product-cart-total > span.cart-price > span.price").text
        tqty = totqty.replace(',', '.')
        TQ= tqty[:-2].encode()

        if float(tot) == round(float(TQ), 1):
            print u'Il prezzo per singolo prodotto si aggiorna correttamente'

        if totqty==driver.find_element_by_css_selector("td.a-right > span.price").text:
            print u"Il subtotale si è correttamente aggiornato"

        print "Verifica sulle spese di spedizione"


        if float(TQ) >=100:
            if driver.find_element_by_css_selector("td.product-cart-total > span.cart-price > span.price").text== totqty:
                print u"Il totale per ordini superiori a 100 euro è corretto"
                print u'La spedizione è gratuita'

        else:
            spedizione= driver.find_element_by_css_selector('#shopping-cart-totals-table > tbody > tr:nth-child(2) > td:nth-child(2)').text
            sped = spedizione.replace(',', '.')
            S = sped[:-2].encode()
            sum= round(float(S), 1)+float(TQ)
            totComplessivo= driver.find_element_by_css_selector("strong > span.price").text
            totC = totComplessivo.replace(',', '.')
            TC = totC[:-2].encode()
            if round(float(TC),1)==float(sum):
                print u"Per ordini inferiori a 100 euro il costo della spedizione è 6,50 euro "
                print u'Il totale complessivo è corretto'



        driver.find_element_by_css_selector("a.btn-remove.btn-remove2 > svg").click()
        time.sleep(4)
        if u"Il carrello è vuoto" == driver.find_element_by_css_selector("h1").text:
            print u"il carrello è stato svuotato correttamente"
        else:
            print u'WARNNG: verifica la corretta rimozione degli articoli dal cart'


    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()