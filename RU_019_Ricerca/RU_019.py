#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_019.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU019(unittest.TestCase):
    def test_Search(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.get(url + "/")
        driver.delete_all_cookies()
        print ("eliminati tutti i cookies")
        print u"Test sulla verifica del funzionanto della search bar"

        driver.implicitly_wait(100)
        driver.find_element_by_css_selector(
            "#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-search.-beopen").click()
        time.sleep(3)
        driver.find_element_by_css_selector("div.input-box > #search").click()
        driver.find_element_by_css_selector("div.input-box > #search").clear()
        time.sleep(5)
        driver.find_element_by_css_selector("div.input-box > #search").send_keys("Tuta")
        print u"Sto effettuando la ricerca per tuta..."
        time.sleep(5)

        suggest=driver.find_elements_by_tag_name('span')
        i=0
        for option_suggest in suggest:
            if option_suggest.get_attribute('class')=='suggestions-productname':
                name=option_suggest.text
                print u"Il prodotto suggerito è: " +name
                i=i+1



        if i==0:
            print U"WARNING: L'autosuggest non ha prodotto risultati"
            driver.quit()

        print "Sto cliccando su un prodotto suggerito"

        for option_suggest in suggest:
            if option_suggest.get_attribute('class') == 'suggestions-productname':
              name1 = option_suggest.text
              option_suggest.click()
              time.sleep(3)
              title = driver.title
              print u"Sei nella pagina del prodotto " + title
              if name1 == driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text:
                print u"Il redirerct alla scheda prodotto è corretto"
              else:
                print u"Il redirect è sbagliato"
              break


    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()