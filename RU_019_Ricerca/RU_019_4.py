#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_019.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU019_3(unittest.TestCase):
    def test_Search(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.get(url + "/")
        driver.delete_all_cookies()
        print ("eliminati tutti i cookies")
        print u"Test sulla verifica del funzionanto della search bar"

        driver.implicitly_wait(100)
        driver.find_element_by_css_selector(
            "#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-search.-beopen").click()
        time.sleep(3)
        driver.find_element_by_css_selector("div.input-box > #search").click()
        driver.find_element_by_css_selector("div.input-box > #search").clear()
        time.sleep(5)
        driver.find_element_by_css_selector("div.input-box > #search").send_keys("ciaoxxx")
        print u"Sto effettuando la ricerca per ciaoxxx..."
        time.sleep(5)

        driver.find_element_by_css_selector('button.button.search-button > span > span').click()

        time.sleep(3)

        try:
            assert "La tua ricerca non ha prodotto risultati." in driver.find_element_by_css_selector('p.note-msg').text
            print u"La tua ricerca non ha prodotto risultati"
        finally:
            time.sleep(2)


    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()