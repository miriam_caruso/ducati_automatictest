#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


class RU003_8(unittest.TestCase):
    def test_SliderCards(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")

        try:
            self.assertEqual("SAFETY GEAR", driver.find_element_by_css_selector("span.category").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertTrue(self.is_element_present(By.XPATH, "//a/picture/img"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("CASUAL WEAR", driver.find_element_by_css_selector(
                "div.swiper-slide.swiper-slide-next > a > span.category").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("//img[@alt='casual wear']")
        try:
            self.assertEqual("ACCESSORIES",
                             driver.find_element_by_xpath("//div/div/div/div/div/div/div/div[3]/a/span").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("//img[@alt='accessories']")
        scroll=driver.find_element_by_css_selector("div.swiper-container.swiper-container-horizontal.swiper-container-free-mode")
        scroll.location_once_scrolled_into_view

        print "Slider cards presenti"



    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()
