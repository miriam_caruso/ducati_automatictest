#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


class RU003_13(unittest.TestCase):
    def test_LinkFooter(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")

        try:
            self.assertEqual("FAQ", driver.find_element_by_link_text("FAQ").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("TERMS OF SALE", driver.find_element_by_link_text("TERMS OF SALE").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("CONTACTS", driver.find_element_by_link_text("CONTACTS").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("RETURNS AND REFUNDS", driver.find_element_by_link_text("RETURNS AND REFUNDS").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(
                u"Copyright © 2018 Ducati Motor Holding S.p.A - Società a Socio Unico - Società soggetta all’attività di Direzione e Coordinamento di AUDI AG. Tutti i diritti riservati. P. IVA 05113870967",
                driver.find_element_by_xpath("//div[4]/section/div[3]").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.box > a.scrambler"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("div.box > a.scrambler").click()
        time.sleep(3)
        try:
            self.assertEqual("Scrambler Ducati | scramblerducati.com", driver.title)

        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.back()
        driver.find_element_by_css_selector("svg.icon.icon--social-fb > use")
        driver.find_element_by_css_selector("svg.icon.icon--social-twitter > use")
        driver.find_element_by_css_selector("svg.icon.icon--social-instagram > use")
        driver.find_element_by_css_selector("svg.icon.icon--social-yt > use")

        try:
            self.assertEqual("ITALIA", driver.find_element_by_xpath("//div[4]/div[3]/a/span").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("FAQ").click()
        time.sleep(3)
        try:
            self.assertEqual("FAQ", driver.title)
        except AssertionError as e:
            self.verificationErrors.append(str(e))

        print "Redirect alla pagina delle FAQ corretto"
        driver.find_element_by_link_text("TERMS OF SALE").click()
        time.sleep(3)
        try:
            self.assertEqual("Terms of sale", driver.title)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print "Redirect alla pagina Terms of sale corretto"
        driver.find_element_by_link_text("CONTACTS").click()
        time.sleep(3)
        try:
            self.assertEqual("Contattaci", driver.title)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print "Redirect alla pagina Contattaci corretto"
        driver.find_element_by_link_text("RETURNS AND REFUNDS").click()
        time.sleep(3)
        try:
            self.assertEqual("RMA Module", driver.title)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("Resi e rimborsi", driver.find_element_by_xpath("//h1").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        print "Redirect alla pagina dedicata al reso corretto"

        print "Verifica effettuata con successo"


    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()