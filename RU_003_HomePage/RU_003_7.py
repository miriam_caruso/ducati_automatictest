#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


class RU003_7(unittest.TestCase):
    def test_HomeSlider(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")

        try:
            self.assertEqual("LIFESTYLE", driver.find_element_by_css_selector(
                "div.swiper-slide.swiper-slide-active > div.d-hero-banner.bluefoot-entity > section.body > div.subtitle").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("Uniche. Come il tuo stile", driver.find_element_by_css_selector(
                "div.swiper-slide.swiper-slide-active > div.d-hero-banner.bluefoot-entity > section.body > h1.title").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector(
            "div.swiper-slide.swiper-slide-active > div.d-hero-banner.bluefoot-entity > section.body").click()
        try:
            self.assertEqual(u"A partire da: 69.90 €", driver.find_element_by_css_selector(
                "div.swiper-slide.swiper-slide-active > div.d-hero-banner.bluefoot-entity > section.body > div.subtxt").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("ACQUISTA ORA", driver.find_element_by_css_selector(
                "div.swiper-slide.swiper-slide-active > div.d-hero-banner.bluefoot-entity > section.body > div.wrapcta.bluefoot-entity > a.d-button.bluefoot-entity > span.txt").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector(
            "div.swiper-button.swiper-button-next._desktop-only > svg > g > circle").click()
        try:
            self.assertEqual(u'MONSTER 1200 / 1200 S',
                             driver.find_element_by_xpath(
                                 "//div[@id='hero-slider-2']/div/div/div[3]/div/section/div").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(u'Pura essenza Monster',
                             driver.find_element_by_xpath(
                                 "//div[@id='hero-slider-2']/div/div/div[3]/div/section/h1").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(
                u'Il nuovo Monster 1200 incarna in pieno i valori\nche fanno battere il cuore dei motociclisti da oltre 20 anni.',
                driver.find_element_by_xpath("//div[@id='hero-slider-2']/div/div/div[3]/div/section/div[2]").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(u'A partire da 13.900 €',
                             driver.find_element_by_xpath(
                                 "//div[@id='hero-slider-2']/div/div/div[3]/div/section/div[3]").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("CONFIGURA ORA", driver.find_element_by_xpath(
                "//div[@id='hero-slider-2']/div/div/div[3]/div/section/div[5]/a/span").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("RICHIEDI TEST RIDE", driver.find_element_by_xpath(
                "//div[@id='hero-slider-2']/div/div/div[3]/div/section/div[5]/a[2]/span").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector(
            "div.swiper-button.swiper-button-next._desktop-only.-dark > svg > g > g > rect").click()
        driver.find_element_by_css_selector(
            "div.swiper-button.swiper-button-next._desktop-only > svg > g > circle").click()

        print "Verifica Home Slider effettuata con successo"

    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()
