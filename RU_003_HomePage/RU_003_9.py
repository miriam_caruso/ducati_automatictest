#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


class RU003_9(unittest.TestCase):
    def test_home(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")
        try:
            self.assertEqual("DUCATI", driver.find_element_by_css_selector("a.anchor.-active").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("DUCATI CORSE", driver.find_element_by_link_text("DUCATI CORSE").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("Si, viaggiare. In stile Ducati",
                             driver.find_element_by_css_selector("li.slide-wrap.-active > div.title").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("SCOPRI LA COLLEZIONE", driver.find_element_by_css_selector("a.cta").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("a.cta").click()
        driver.find_element_by_xpath("//img[@alt='Ducati Shop']").click()
        driver.find_element_by_link_text("DUCATI CORSE").click()
        driver.find_element_by_xpath("//li[2]/picture/img").click()
        try:
            self.assertEqual("Ducati Corse 2018",
                             driver.find_element_by_css_selector("li.slide-wrap.-active > div.title").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("li.slide-wrap.-active > a.cta").click()

        print "Sezione Home - Si, viaggiare. In stile Ducati verificata con successo"



    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()