#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()

class RU003_5(unittest.TestCase):
    def test_AbbigliamentoCasual(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")

        time.sleep(2)

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        try:
            self.assertEqual("ABBIGLIAMENTO CASUAL", driver.find_element_by_xpath("//div/div/div[2]/div/a").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("#cat-20").click()
        time.sleep(2)
        i = 0
        j = 8

        all_option = driver.find_elements_by_tag_name('a')
        for categories in all_option:
            if categories.get_attribute('class')=='link':
                categorieOnSite = open('AbbigliamentoCasual.txt')
                CoS = categorieOnSite.readlines()
                for categorie in CoS:
                    categorie.splitlines(1)
                    cat = categories.get_attribute('innerHTML')
                    if cat + '\n'==categorie.decode():
                        print "Nella subcategoria ABBIGLIAMENTO MOTO, la categoria " +cat + u' è presente'
                        i=i+1
                        break

        if (i==j):
            print "Tutte le sotto-categorie sono presenti"
        else:
            print "WARNING: manca qualche sotto-categoria"

        driver.find_element_by_css_selector("#cat-21").click()
        time.sleep(3)
        driver.find_element_by_css_selector("#cat-20").click()
        driver.find_element_by_css_selector("#cat-22").click()
        time.sleep(3)
        driver.find_element_by_css_selector("#cat-20").click()
        driver.find_element_by_css_selector("#cat-23").click()
        time.sleep(3)
        driver.find_element_by_css_selector("#cat-20").click()
        driver.find_element_by_css_selector("#cat-24").click()
        time.sleep(3)
        driver.find_element_by_css_selector("#cat-20").click()
        driver.find_element_by_css_selector("li.link.-scrambler.-active").click()


    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()