#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os

reload(sys)
sys.getdefaultencoding()


class RU003_10(unittest.TestCase):
    def test_Banner(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")

        try:
            self.assertEqual("World Ducati Week 2018",
                             driver.find_element_by_css_selector("div.content > h2.title").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(
                u"Il conto alla rovescia per il più grande raduno Ducati del mondo è iniziato! Appuntamento a Misano dal 20 al 22 luglio.",
                driver.find_element_by_css_selector("p.text").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(u"SCOPRI DI PIÙ", driver.find_element_by_css_selector(
                "div.body > div.wrapcta > a.d-button.bluefoot-entity > span.txt").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertTrue(self.is_element_present(By.XPATH, "//div[4]/div/div/div/picture/img"))
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("//div[4]/div/div/div/div[2]/a/span").click()

        print "Verifica Banner in Home Page effettuata con successo"


    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()