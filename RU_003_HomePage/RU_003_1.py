#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_003_1.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)

class RU003_1(unittest.TestCase):
    def test_HeaderFooter(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(5)
        driver.get(url + "/")

        time.sleep(9)
        # browser.find_element_by_css_selector("#nw_close>svg").click()

        try: self.assertEqual("SCOPRI LA COLLEZIONE", driver.find_element_by_css_selector("span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Spedizione gratuita per tutti gli ordini da € 100", driver.find_element_by_css_selector("p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("ITALIA", driver.find_element_by_css_selector("span.label").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("ABBIGLIAMENTO MOTO", driver.find_element_by_css_selector("#cat-10").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("ABBIGLIAMENTO CASUAL", driver.find_element_by_css_selector("#cat-20").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("ACCESSORI", driver.find_element_by_css_selector("#cat-29").text)
        except AssertionError as e: self.verificationErrors.append(str(e))

        driver.find_element_by_xpath("//section/div").click()
        time.sleep(2)
        driver.find_element_by_css_selector("div.hamburger").click()
        driver.find_element_by_css_selector("img[alt=\"Ducati Shop\"]")
        driver.find_element_by_css_selector("li.link.top-search > a > svg")
        browser.find_element_by_css_selector("li.link.top-user.-mobilewasteful>a>svg")
        driver.find_element_by_css_selector("svg > g > path")

        print "Tutti i contenuti dell'header sono presenti"

        # scroll della pagina
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(3)
        driver.find_element_by_xpath("//section/div").click()
        time.sleep(2)
        driver.find_element_by_css_selector("div.hamburger").click()
        driver.find_element_by_css_selector("li.link.top-search > a > svg")
        driver.find_element_by_css_selector('li.link.top-user.-mobilewasteful > a')
        driver.find_element_by_css_selector("svg > g > path")

        print "Tutti i contenuti dell'header sticky sono presenti"


        try:
            self.assertEqual("FAQ", driver.find_element_by_link_text("FAQ").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("TERMS OF SALE", driver.find_element_by_link_text("TERMS OF SALE").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("CONTACTS", driver.find_element_by_link_text("CONTACTS").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual("RETURNS AND REFUNDS", driver.find_element_by_link_text("RETURNS AND REFUNDS").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        try:
            self.assertEqual(
                u"Copyright © 2018 Ducati Motor Holding S.p.A - Società a Socio Unico - Società soggetta all’attività di Direzione e Coordinamento di AUDI AG. Tutti i diritti riservati. P. IVA 05113870967",
                driver.find_element_by_xpath("//div[4]/section/div[3]").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("div.box > a.scrambler > svg.icon.icon--logo-scrambler")
        driver.find_element_by_css_selector("svg.icon.icon--social-fb")
        driver.find_element_by_css_selector("svg.icon.icon--social-twitter > use")
        driver.find_element_by_css_selector("svg.icon.icon--social-instagram")
        driver.find_element_by_css_selector("svg.icon.icon--social-yt > use")
        try:
            self.assertEqual("ITALIA", driver.find_element_by_css_selector("div.box > a.country > span.label").text)
        except AssertionError as e:
            self.verificationErrors.append(str(e))

        print "I contenuti del footer sono presenti"

    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()
