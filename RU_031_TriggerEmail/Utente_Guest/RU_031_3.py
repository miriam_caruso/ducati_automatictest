#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from RU_031_TriggerEmail.Utente_Guest.mailinator import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, shutil, ConfigParser

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_031_3.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU_031_3(unittest.TestCase):
    def test_OrdineAnnullato(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver= mailnator
        driver.get(url_email + "/")
        print u"Test sulla verifica della ricezione dell'email in caso di ordine annullato"



        config = ConfigParser.RawConfigParser()
        config.read('ConfigFile.properties')
        print u"I dati dell'utente sono:"

        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print u"Nome: " +nome
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print u"Cognome: " +cognome
        password = config.get('SEZIONE_PASSWORD', 'password')
        print u"Password: "  +password
        email = config.get('SEZIONE_EMAIL', 'email')
        print u"Email: " +email
        via = config.get('SEZIONE_DATI_UTENTE', 'via2')
        print "Via: " + via
        via1 = config.get('SEZIONE_SPEDIZIONE', 'via1')
        print "Via: " + via1
        cap = config.get('SEZIONE_DATI_UTENTE', 'zip')
        print "Cap: " + cap
        telefono = config.get('SEZIONE_DATI_UTENTE', 'telefono')
        print "Telefono: " + telefono
        codice = config.get('SEZIONE_DATI_UTENTE', 'codice')
        print "Codice: " + codice
        cap2 = config.get('SEZIONE_SPEDIZIONE', 'zip2')
        print "Cap: " + cap2
        telefono2 = config.get('SEZIONE_SPEDIZIONE', 'telefono2')
        print "Telefono: " + telefono2
        ordine = config.get('SEZIONE_ORDINE', 'numero_ordine')
        print "Numero ordine: " + ordine





        time.sleep(5)


        option= driver.find_elements_by_tag_name("div")

        for all_message in option:
            if all_message.get_attribute('class')== 'all_message-min_text all_message-min_text-3':
                if all_message.text== u'Il tuo ordine Ducati n. 23310000111-2 è stato cancellato':
                    time.sleep(2)
                    all_message.click()




        time.sleep(3)
        driver.switch_to.frame("msg_body")

        time.sleep(3)

        try:
           assert "Ciao Ospite" in  driver.find_element_by_css_selector("td.container-table--inner.top-content > table > tbody > tr > td > p").text
           print u"L'email per l'ordine cancellato è arrivata correttamente"
           print u'Verifica del contenuto in corso'
        finally:
            time.sleep(2)

        driver.find_element_by_xpath("//p[2]").text

        driver.find_element_by_xpath("//p[3]").text


        try:
            assert u"Il Servizio Clienti sarà felice di assisterti!" in  driver.find_element_by_xpath("//p[4]").text
        finally:
            time.sleep(1)

        try:
            assert u'enabled by Alkemy s.p.a.\nC.f.e.p.i 05619950966 reg. delle imprese di Milano - Cap Soc. 379.961 euro i.v' in driver.find_element_by_css_selector(
                "td.footer-intro > p").text
            print ""
        finally:
            time.sleep(1)


        driver.find_element_by_css_selector("img[alt=\"Ducati su Facebook\"]").click()
        driver.find_element_by_xpath("//a[2]/img").click()
        driver.find_element_by_xpath("//a[4]/img").click()
        driver.find_element_by_xpath("//a[5]/img").click()

        print "Verifica effettuata con successo"



    def is_element_present(self, how, what):
        try:
            mailnator.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            mailnator.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert =mailnator.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        mailnator.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()