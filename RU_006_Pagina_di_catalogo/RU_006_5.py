#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()


nomeTest = 'RU_006_5.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU006_5(unittest.TestCase):
    def test_FiltriWomenMan(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        print 'click sul tasto hamburger'
        driver.find_element_by_xpath("(//a[contains(text(),'Abbigliamento Casual')])[3]").click()
        time.sleep(3)
        print 'click sulla categoria abigliamento casual'
        try:
            assert "KID" in driver.find_element_by_css_selector("a.amshopby-attr").text
            print 'il filtro KID è presente'
        finally:
            time.sleep(2)
        try:
            assert "WOMAN" in  driver.find_element_by_xpath("//dl[@id='narrow-by-list']/table/tbody/tr/td/dd/ol/li[2]/div/a").text
            print 'il filtro WOMAN è presente'
        finally:
            time.sleep(2)
        driver.find_element_by_css_selector("a.amshopby-attr").click()
        time.sleep(3)
        print 'click sulla categoria KID per togliere il filtro, e reinderizza sulla pagina contenente i prodotti WOMAN'
        driver.find_element_by_css_selector("a.amshopby-attr.filter-item-selected").click()
        time.sleep(5)
        print 'click sulla categoria WOMAN, per togliere il filtro, e reinderizza sulla pagina contenente i prodotti KID '











        def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

        def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

        def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

        def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

        if __name__ == "__main__":
            unittest()







