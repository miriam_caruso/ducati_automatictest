#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()



nomeTest = 'RU_006_7.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU006_7(unittest.TestCase):
    def test_EliminazioneFiltri(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        print 'click hamburger'
        driver.find_element_by_css_selector("a[title=\"Giacche In Pelle\"]").click()
        time.sleep(3)
        print ' click sulla categoria giacca in pelle'
        driver.find_element_by_css_selector("#filters-dropdown > span").click()
        time.sleep(3)
        print 'click sul FILTRA PER'
        driver.switch_to_active_element()
        driver.find_element_by_css_selector("div.wrapper-content.filters-content.opened").click()
        time.sleep(3)
        print 'switch div filtri colori'
        driver.find_element_by_id("filter-item_macrocolor432").click()
        time.sleep(3)
        print ' click sul colore black'
        driver.find_element_by_css_selector("div.wrapper-content.filters-content.opened").click()
        time.sleep(3)
        print 'switch div filtri manufacturer'
        driver.find_element_by_css_selector("#filter-item_manufacturer241").click()
        time.sleep(3)
        print 'click sul manufacturer'
        driver.switch_to_active_element()
        driver.find_element_by_css_selector("div.wrapper-content.filters-content.opened").click()
        time.sleep(3)
        print 'switch sul div filtri colori'
        driver.find_element_by_id("filter-item_macrocolor432").click()
        time.sleep(3)
        print ' click per eliminare il check sul colore black'
        driver.find_element_by_css_selector("div.wrapper-content.filters-content.opened").click()
        time.sleep(3)
        print ' swicht sul div filtra per'
        driver.find_element_by_css_selector("#filter-item_manufacturer241").click()
        time.sleep(3)
        print ' click per eliminare il check sul manufacturer'
        driver.switch_to_active_element()
        driver.find_element_by_css_selector("#filters-dropdown > span").click()
        print 'click sulla chiusira dei filtri'
