#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os
reload(sys)
sys.getdefaultencoding()

nomeTest = 'RU_006_6.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU006_6(unittest.TestCase):
    def test_SelezioneFiltri(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(3)
        driver.find_element_by_css_selector("a[title=\"Giacche In Pelle\"]").click()
        time.sleep(3)
        driver.find_element_by_css_selector("#filters-dropdown > span").click()
        time.sleep(3)
        driver.switch_to_active_element()
        driver.find_element_by_css_selector("div.wrapper-content.filters-content.opened").click()
        time.sleep(3)
        driver.find_element_by_id("filter-item_macrocolor432").click()
        time.sleep(3)
        driver.find_element_by_css_selector("div.wrapper-content.filters-content.opened").click()
        driver.find_element_by_css_selector("#filter-item_manufacturer241").click()

        try:
            assert "MACROCOLOR" in driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/div/div[1]/div[1]/div/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div/h6/a').text
            print 'Macrocolor presente'
        finally:
            time.sleep(2)
        try:
            assert "MANUFACTURER" in driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div/div/div[1]/div[1]/div/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div/h6/a').text
            print 'MANUFACTURER presente'
        finally:
            time.sleep(2)

        driver.find_element_by_css_selector("button.d-button.-white.btn-apply > span.txt").click()
        all_option = browser.find_elements_by_tag_name('div')
        for product in all_option:
            if product.get_attribute('class') == 'd-card-imgtext -products':
                nameP = product.text
                print u"Sono presenti i seguenti prodotti con filtro black e Dainese :" + nameP


        def is_element_present(self, how, what):
            try:
                browser.find_element(by=how, value=what)
            except NoSuchElementException as e:
                return False
            return True

        def is_alert_present(self):
            try:
                browser.switch_to_alert()
            except NoAlertPresentException as e:
                return False
            return True

        def close_alert_and_get_its_text(self):
            try:
                alert = browser.switch_to_alert()
                alert_text = alert.text
                if self.accept_next_alert:
                    alert.accept()
                else:
                    alert.dismiss()
                return alert_text
            finally:
                self.accept_next_alert = True

        def tearDown(self):
            browser.quit()
            self.assertEqual([], self.verificationErrors)

        if __name__ == "__main__":
            unittest()



