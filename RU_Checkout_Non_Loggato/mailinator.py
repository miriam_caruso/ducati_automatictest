#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from selenium import webdriver


from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import unittest, time, re, os, sys, ConfigParser, string, random

string.letters
'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

letter=''.join(random.choice(string.letters) for x in range(4))

email=letter + 'testDucati'

config = ConfigParser.RawConfigParser()
config.read('ConfigFile.properties')

config.set('SEZIONE_EMAIL', 'email', email + '@mailinator.com')

print email + u'@mailinator'

mailnator=  webdriver.Chrome()
mailnator.implicitly_wait(5)


mailnator.get("https://www.mailinator.com/")
mailnator.maximize_window()
mailnator.find_element_by_id("inboxfield").click()
time.sleep(5)
mailnator.find_element_by_id("inboxfield").send_keys(email)
mailnator.find_element_by_css_selector('button.btn.btn-dark').click()


url_email=mailnator.current_url
config.set('SEZIONE_EMAIL', 'url_email',url_email)


with open('ConfigFile.properties', 'w') as configfile:
    config.write(configfile)


print url_email

