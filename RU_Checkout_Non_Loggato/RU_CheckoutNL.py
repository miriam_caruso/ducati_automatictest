#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from RU_Checkout_Non_Loggato.mailinator import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, shutil, ConfigParser

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_CheckoutNL.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU025_1(unittest.TestCase):
    def test_CheckoutNL(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        second_driver= mailnator
        driver.get(url + "/")
        print u"Test sulla verifica del checkout non loggato"



        config = ConfigParser.RawConfigParser()
        config.read('ConfigFile.properties')
        print u"I dati dell'utente sono:"

        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print u"Nome: " +nome
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print u"Cognome: " +cognome
        password = config.get('SEZIONE_PASSWORD', 'password')
        print u"Password: "  +password
        email = config.get('SEZIONE_EMAIL', 'email')
        print u"Email: " +email
        via = config.get('SEZIONE_DATI_UTENTE', 'via2')
        print "Via: " + via
        via1 = config.get('SEZIONE_SPEDIZIONE', 'via1')
        print "Via: " + via1
        cap = config.get('SEZIONE_DATI_UTENTE', 'zip')
        print "Cap: " + cap
        telefono = config.get('SEZIONE_DATI_UTENTE', 'telefono')
        print "Telefono: " + telefono
        codice = config.get('SEZIONE_DATI_UTENTE', 'codice')
        print "Codice: " + codice
        cap2 = config.get('SEZIONE_SPEDIZIONE', 'zip2')
        print "Cap: " + cap2
        telefono2 = config.get('SEZIONE_SPEDIZIONE', 'telefono2')
        print "Telefono: " + telefono2

        print 'click sul tasto hamburger'
        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(4)
        driver.find_element_by_xpath("//div[@id='d-header-main']/div/section/section[3]/div/div/div/div/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        print "redirect alla scheda prodotto"
        time.sleep(4)
        name1 = driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
        print u"Il nome del prodotto è: " + name1
        price1 = driver.find_element_by_css_selector('span.price > span.price').text
        pr1 = price1.replace(',', '.')
        p1 = pr1[:-2].encode()




        driver.find_element_by_xpath("//button[@id='addtocartbtn']/span").click()
        print u'Il prodotto 1 è stato aggiuto al carrello'
        time.sleep(4)
        driver.get(url)
        time.sleep(3)
        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(4)
        driver.find_element_by_xpath("//div/div/div[2]/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        name2 = driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
        print u"Il nome del prodotto è: " + name2
        price2 = driver.find_element_by_css_selector('span.price > span.price').text
        pr2 = price2.replace(',', '.')
        p2 = pr2[:-2].encode()
        driver.find_element_by_css_selector("#addtocartbtn > span.txt").click()
        time.sleep(9)
        print u'Il prodotto 2 è stato aggiunto al carrello'
        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(4)
        driver.find_element_by_xpath("//div[3]/ul/li/a").click()
        driver.find_element_by_css_selector("a.picturewrap.product-image > picture.picture").click()
        name3 = driver.find_element_by_css_selector('div.product-name._hide-small-mobile > span.h1').text
        print u"Il nome del prodotto è: " + name3
        price3 = driver.find_element_by_css_selector('span.price > span.price').text
        pr3 = price3.replace(',', '.')
        p3 = pr3[:-2].encode()
        driver.find_element_by_css_selector("#addtocartbtn > span.txt").click()
        print u'Il prodotto 3 è stato aggiunto al carrello'

        time.sleep(5)

        driver.find_element_by_css_selector("span.icon.no-empty").click()


        time.sleep(3)

        size= driver.find_element_by_css_selector('#cart-sidebar > li:nth-child(2) > div.product-details > div.info-wrapper > div.options-wrapper > div > div > div.attribute-value')

        tot = float(p1)+float(p2)+float(p3)

        driver.find_element_by_xpath('//li[4]/a/span/span[2]').click()

        time.sleep(3)

        driver.find_element_by_css_selector('a.d-button.btn-proceed-checkout.btn-checkout').click()

        if tot>= 100:

            subtotal=driver.find_element_by_css_selector("#shopping-cart-totals-table > tfoot > tr > td.a-right.grantotal.last > strong > span").text
            subt = subtotal.replace(',', '.')
            sub = subt[:-2].encode()

            if round(float(sub) , 1) == tot:
                print u"Il totale per ordini superiori a 100 euro è corretto"
                print u'La spedizione è gratuita'

        else:
            spedizione = driver.find_element_by_css_selector(
                '#shopping-cart-totals-table > tbody > tr:nth-child(2) > td:nth-child(2)').text
            sped = spedizione.replace(',', '.')
            S = sped[:-2].encode()
            sum = round(float(S), 1) + float(tot)
            totComplessivo = driver.find_element_by_css_selector("strong > span.price").text
            totC = totComplessivo.replace(',', '.')
            TC = totC[:-2].encode()
            if round(float(TC), 1) == sum:
                print u"Per ordini inferiori a 100 euro il costo della spedizione è 6,50 euro "
                print u'Il totale complessivo è corretto'


        driver.find_element_by_css_selector('body > div.global-wrapper.page-wrapper > div > div.main > div > div.d-cart-table._generic-container > div > div.cart-bottom > div.cart-totals-wrapper > div > div > ul > li > button').click()

        time.sleep(3)
        try:
            assert "Accedi" in driver.find_element_by_css_selector("div.std > h4").text
            print 'il titolo Accedi è presente'
        finally:
            time.sleep(2)
        try:
            assert u"Inserisci i tuoi dati di accesso per poter procedere con l’acquisto." in driver.find_element_by_css_selector(
                "p").text
            print 'il testo inserisci i tuoi dati..  è presente'
        finally:
            time.sleep(2)

        try:
            assert "Hai dimenticato la password?" in driver.find_element_by_css_selector("a.f-pwd").text
            print 'il titolo Hai dimenticato la password? è presente'
        finally:
            time.sleep(2)
        try:
            assert "ACCEDI" in driver.find_element_by_css_selector("button.d-button.-gray").text
            print 'il bottone ACCEDI è presente'
        finally:
             time.sleep(2)
        # try:
        #     assert u"ACCEDI CON FACEBOOK" in driver.find_element_by_xpath("//span[2]").text
        #     print 'Accedi con FB è presente'
        # finally:
        #     time.sleep(2)
        print 'la sezione Accedi è presente'

        try:
            assert 'Nuovo utente' in driver.find_element_by_css_selector("div.block-checkout-new>div.std>h4").text
            print  'Nuovo utente è presente'
        finally:
            time.sleep(2)
        try:
            assert "Creando un account con il nostro negozio sarai in grado di muoverti velocemente verso gli ordini, effettuare ordini con indirizzi di spedizione multipli, visualizzare e tracciare i tuoi ordini nel tuo account" in driver.find_element_by_css_selector(
                "div.block-checkout-new > div.std > p").text
            print 'il testo creando... è presente'
        finally:
            time.sleep(2)
        try:
            assert "REGISTRATI" in driver.find_element_by_css_selector("#onepage-guest-register-button > span.txt").text
            print 'il bottone Registrati è presente'
        finally:
            time.sleep(2)
        try:
            assert "Continua come ospite" in driver.find_element_by_css_selector(
                "div.block-checkout-login_guest > div.std > h4").text
            print 'Continua come ospite è presente'
        finally:
            time.sleep(2)
        try:
            assert u"Inserisci i tuoi dati di accesso per poter procedere con l’acquisto." in driver.find_element_by_css_selector(
                "div.block-checkout-login_guest > div.std > p").text
            print 'Inserisci i tuoi dati.. è presente'
        finally:
            time.sleep(2)
        try:
            assert "CONTINUA COME OSPITE" in driver.find_element_by_css_selector("#onepage-guest-guest-button").text
            print  " il bottone Continua come ospite è presente"
        finally:
            time.sleep(2)

        print "Acquisto come ospite"

        driver.find_element_by_css_selector("#onepage-guest-guest-button > span.txt").click()

        time.sleep(3)

        if driver.title == 'Checkout' :
            print u"Il redirect alla pagina del checkout è corretto"

        if "I tuoi dati"== driver.find_element_by_css_selector("h4.step-title").text:
            print u"Sei nel primo step del checkout"
        if "Dati di Fatturazione"== driver.find_element_by_css_selector("h4.first-subtitle").text:
            print "Sezione relativa ai dati di fatturazione presente"
        if 'Desideri ricevere la fattura?'== driver.find_element_by_css_selector("div.shipping_information").text:
            print 'sezione relativa alla fattura presente'

        if "Leggi l'informativa al trattamento dei dati personali prima di procedere con la registrazione. Informativa sulla privacy"== driver.find_element_by_css_selector("h5").text:
            print "Sezione relativa alla GDPR presente"
        if "Dati di Spedizione" == driver.find_element_by_css_selector("li.shipping-fields > div > h4.first-subtitle").text:
            print "Sezione relativa ai dati di spedizione presente"
        if ("Spedisci a questo indirizzo" == driver.find_element_by_css_selector( "div.control > span.d-radio-button > label.label").text and
            "Spedisci ad un indirizzo diverso"== driver.find_element_by_css_selector("div.control > div.control > span.d-radio-button > label.label").text and
             "Ricevi il tuo acquisto in uno dei punti di ritiro Fermo!Point (www.fermopoint.it)" == driver.find_element_by_css_selector("div.control.controls-radio > span.d-radio-button > label.label").text):
             print "Fermo!Point presente"

        #VERIFICA PRESENZA MESSAGGI DI WARNING

        driver.find_element_by_css_selector("button.d-button.right > span.txt").click()
        if u"Questo è un campo obbligatorio." == driver.find_element_by_id("advice-required-entry-billing:firstname").text:
            print "Il messaggio di warning su nome compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio NOME in caso di mancata compilazione"


        if u"Questo è un campo obbligatorio."== driver.find_element_by_id("advice-required-entry-billing:lastname").text:
            print "Il messaggio di warning su cognome compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio COGNNOME in caso di mancata compilazione"


        if u"Questo è un campo obbligatorio." == driver.find_element_by_id("advice-required-entry-billing:email").text:
            print "Il messaggio di warning su EMAIL compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio EMAIL in caso di mancata compilazione"

        if "Selezionare una opzione."== driver.find_element_by_id("advice-validate-select-billing:region_id").text:
            print "Il messaggio di warning su PROVINCIA compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio PROVINCIA in caso di mancata compilazione"

        if u"Questo è un campo obbligatorio."== driver.find_element_by_id("advice-required-entry-billing:postcode").text:
            print "Il messaggio di warning su CAP compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio CAP in caso di mancata compilazione"

        if "Selezionare una delle opzioni." == driver.find_element_by_id("advice-validate-one-required-by-name-want_invoice_no").text:
            print "Il messaggio di warning sulla scelta della ricezione o meno della fattura compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio relativo alla ricezione o meno della fattura in caso di mancata compilazione"

        if ("Selezionare una delle opzioni."== driver.find_element_by_id("advice-validate-one-required-by-name-global-privacy-field-1-agree-yes").text and
            "Selezionare una delle opzioni.", driver.find_element_by_id("advice-validate-one-required-by-name-global-privacy-field-1-agree-no").text and
            "Selezionare una delle opzioni.", driver.find_element_by_id("advice-validate-one-required-by-name-global-privacy-field-2-agree-yes").text and
            "Selezionare una delle opzioni.", driver.find_element_by_id("advice-validate-one-required-by-name-global-privacy-field-2-agree-no").text):

            print "Il messaggio di warning sulla GDPR compaiono correttamente"
        else:
            print u"WARNING: non compare il messaggio sui campi obbligatori relativi alla GDPR in caso di mancata compilazione"


        subtotaleCheck= driver.find_element_by_css_selector('dd.value > span.price').text

        subCheck = subtotaleCheck.replace(',', '.')
        SubC= subCheck[:-2].encode()

        SC=float(tot)/1.22

        totaleCheck =driver.find_element_by_css_selector('dd.value.total > span.price').text
        totCheck = totaleCheck .replace(',', '.')
        totC = totCheck[:-2].encode()

        if tot >= 100:
            if (round (float(SC) , 1)==round(float(SubC) , 1) and
                round(float(tot), 1) == round(float(totC), 1)):
              print u"Totale e subtotale corretti"
              print u"L'iva è calcolata correttamente"
            else:
              print u"WARNING: Verifica la correttezza del totale e subtotale"

        else:
            if (round(float(SC), 1) == round(float(SubC), 1) and
              float(tot)==float(totC)+6.50):
                print u"Totale e subtotale corretti"
                print u"L'iva è calcolata correttamente"
            else:
                print u"WARNING: Verifica la correttezza del totale e subtotale"

        #VERIFICA FUNZIONAMENTO PULSANTE MODIFICA CARRELLO

        print "Verifica funzionamento pulsante MODIFICA IL CARRELLO"

        driver.find_element_by_link_text("MODIFICA IL CARRELLO").click()
        try:
            assert "Carrello" in  driver.title
            print "Redirect al carrello corretto"
        finally:
            time.sleep(2)


        driver.find_element_by_css_selector("button.d-button.btn-proceed-checkout.btn-checkout").click()
        driver.find_element_by_css_selector("#onepage-guest-guest-button > span.txt").click()
        try:
            assert "Checkout" in driver.title
            print "Redirect al Checkout corretto"
        finally:
            time.sleep(2)

        driver.find_element_by_id("billing:firstname").click()
        driver.find_element_by_id("billing:firstname").clear()
        driver.find_element_by_id("billing:firstname").send_keys(nome)
        driver.find_element_by_id("billing:lastname").click()
        driver.find_element_by_id("billing:lastname").clear()
        driver.find_element_by_id("billing:lastname").send_keys(cognome)
        driver.find_element_by_id("billing:email").click()
        driver.find_element_by_id("billing:email").clear()
        driver.find_element_by_id("billing:email").send_keys(email)
        driver.find_element_by_id("billing:street1").click()
        driver.find_element_by_id("billing:street1").clear()
        driver.find_element_by_id("billing:street1").send_keys(via)

        driver.find_element_by_xpath("//div[2]/div/div/span/span/span[2]").click()
        driver.find_element_by_xpath("//div[2]/div/div/ul/li[7]").click()




        provincia = driver.find_element_by_xpath("//div[2]/div/div/ul/li[7]").get_attribute('innerHTML')

        driver.find_element_by_xpath('//*[@id="billing-new-address-form"]/fieldset/ul/li[5]/div[1]/div/div/span/span/span[1]').click()
        driver.find_element_by_xpath("//li[5]/div/div/div/ul/li[13]").click()

        citta = driver.find_element_by_xpath("//li[5]/div/div/div/ul/li[13]").get_attribute('innerHTML')

        config.set('SEZIONE_DATI_UTENTE', 'provincia', provincia)
        config.get('SEZIONE_DATI_UTENTE', 'provincia')

        config.set('SEZIONE_DATI_UTENTE', 'citta', citta)
        config.get('SEZIONE_DATI_UTENTE', 'citta')

        with open('ConfigFile.properties', 'w') as configfile:
            config.write(configfile)


        driver.find_element_by_id("billing:postcode").click()
        driver.find_element_by_id("billing:postcode").clear()
        driver.find_element_by_id("billing:postcode").send_keys(cap)

        driver.find_element_by_id("billing:telephone").click()
        driver.find_element_by_id("billing:telephone").clear()
        driver.find_element_by_id("billing:telephone").send_keys(telefono)

        driver.find_element_by_xpath(
            "//li[@id='billing-new-address-form']/fieldset/ul/li[9]/ul/li[2]/span/label").click()

        # driver.find_element_by_css_selector("label.label.required").click()
        # driver.find_element_by_id("billing:tax_code").click()
        # driver.find_element_by_id("billing:tax_code").clear()
        # driver.find_element_by_id("billing:tax_code").send_keys(codice)
        driver.find_element_by_xpath("//ul[@id='global-agreements']/li/div/div/span/label").click()
        driver.find_element_by_xpath("//ul[@id='global-agreements']/li[2]/div/div/span/label").click()

        try:

            assert "Vorrei creare un account" in  driver.find_element_by_css_selector("label.label").text
            print "Check Vorrei creare un account presente "
        finally:
            time.sleep(2)

        driver.find_element_by_css_selector("div.control > div.control > span.d-radio-button > label.label").click()
        driver.find_element_by_css_selector("button.d-button.right > span.txt").click()

        time.sleep(7)



        #VERIFICA MESSAGGI DI WARNING IN CASO DI INDIRIZZO DI FATTURAZIONE DIVERSO DA QUELLO DI SPEDIZONE

        print u"VERIFICA MESSAGGI DI WARNING IN CASO DI INDIRIZZO DI FATTURAZIONE DIVERSO DA QUELLO DI SPEDIZONE "


        if u"Questo è un campo obbligatorio." == driver.find_element_by_id(
                "advice-required-entry-shipping:firstname").text:
            print "Il messaggio di warning su nome compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio NOME in caso di mancata compilazione"

        if u"Questo è un campo obbligatorio." == driver.find_element_by_id(
                "advice-required-entry-shipping:lastname").text:
            print "Il messaggio di warning su cognome compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio COGNNOME in caso di mancata compilazione"

        if u"Questo è un campo obbligatorio." == driver.find_element_by_id("advice-required-entry-shipping:street1").text:
            print "Il messaggio di warning su indirizzo compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio INDIRIZZO in caso di mancata compilazione"

        if "Selezionare una opzione." == driver.find_element_by_id("advice-validate-select-shipping:region_id").text:
            print "Il messaggio di warning su PROVINCIA compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio PROVINCIA in caso di mancata compilazione"

        if u"Questo è un campo obbligatorio." == driver.find_element_by_id(
                "advice-required-entry-shipping:postcode").text:
            print "Il messaggio di warning su CAP compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio CAP in caso di mancata compilazione"

        if u"Questo è un campo obbligatorio." ==driver.find_element_by_id("advice-required-entry-shipping:city").text:
            print u"Il messaggio di warning su città compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio CITTÀ in caso di mancata compilazione"

        if u"Questo è un campo obbligatorio." == driver.find_element_by_id("advice-required-entry-shipping:telephone").text:
            print u"Il messaggio di warning su TELEFONO compare correttamente"
        else:
            print u"WARNING: non compare il messaggio sul campo obbligatorio TELEFONO in caso di mancata compilazione"


        #COMPILAZIONE INDIRIZZO DI SPEDIZIONE

        driver.find_element_by_id("shipping:firstname").click()
        driver.find_element_by_id("shipping:firstname").clear()
        driver.find_element_by_id("shipping:firstname").send_keys(nome)
        driver.find_element_by_id("shipping:lastname").click()
        driver.find_element_by_id("shipping:lastname").clear()
        driver.find_element_by_id("shipping:lastname").send_keys(cognome)

        driver.find_element_by_id("shipping:street1").click()
        driver.find_element_by_id("shipping:street1").clear()
        driver.find_element_by_id("shipping:street1").send_keys(via1)

        driver.find_element_by_xpath('//*[@id="shipping-new-address-form"]/fieldset/div[4]/div[2]/div/div/span/span/span[1]').click()
        driver.find_element_by_xpath('//*[@id="shipping-new-address-form"]/fieldset/div[4]/div[2]/div/div/ul/li[16]').click()

        time.sleep(2)

        provincia2 = driver.find_element_by_xpath('//*[@id="shipping-new-address-form"]/fieldset/div[4]/div[2]/div/div/ul/li[16]').get_attribute('innerHTML')

        driver.find_element_by_xpath(
            '//*[@id="shipping-new-address-form"]/fieldset/div[4]/div[3]/div/div[1]/span/span/span[1]').click()
        driver.find_element_by_xpath('//*[@id="shipping-new-address-form"]/fieldset/div[4]/div[3]/div/div[1]/ul/li[54]').click()

        citta2 = driver.find_element_by_xpath('//*[@id="shipping-new-address-form"]/fieldset/div[4]/div[3]/div/div[1]/ul/li[54]').get_attribute('innerHTML')

        config.set('SEZIONE_SPEDIZIONE', 'provincia2', provincia2)
        config.get('SEZIONE_SPEDIZIONE', 'provincia2')

        config.set('SEZIONE_SPEDIZIONE', 'citta2', citta2)
        config.get('SEZIONE_SPEDIZIONE', 'citta2')

        with open('ConfigFile.properties', 'w') as configfile:
            config.write(configfile)

        driver.find_element_by_id("shipping:postcode").click()
        driver.find_element_by_id("shipping:postcode").clear()
        driver.find_element_by_id("shipping:postcode").send_keys(cap2)

        driver.find_element_by_id("shipping:telephone").click()
        driver.find_element_by_id("shipping:telephone").clear()
        driver.find_element_by_id("shipping:telephone").send_keys(telefono2)

        driver.find_element_by_css_selector('#billing-new-address-form > fieldset > ul > li.control.control-invoice > ul > li:nth-child(3) > span > label').click()

        #driver.find_element_by_css_selector("label.label.required").click()
        # driver.find_element_by_id("shipping:tax_code").click()
        # driver.find_element_by_id("shipping:tax_code").clear()

        # driver.find_element_by_css_selector(
        #     "#co-shipping-form > div.control > span > label").click()

        time.sleep(2)
        driver.find_element_by_css_selector("button.d-button.right > span.txt").click()

        #STEP 2 METODO DI SPEDIZIONE E PAGAMENTO

        time.sleep(4)
        try:
           assert "Metodi di spedizione" in driver.find_element_by_css_selector("#checkout-shipping-method-load > h4").text
           print u"Passaggio allo step successivo del checkout relativo alla spedizione"
        finally:
            time.sleep(2)

        if tot >= 100:
            if u'Spedizione Gratuita 0,00 €' == driver.find_element_by_css_selector(
                "dd.shipping-message-container > span.d-radio-button > label").text:
                print "Spedizione gratuita presente"
        else:
            if u"Spedizione Standard 6,50 €" == driver.find_element_by_css_selector(
                "dd.shipping-message-container > span.d-radio-button > label").text:
               print "Spedizione standard presente"

        try:
            assert u"Spedizione Su Appuntamento + 4,90 €" in driver.find_element_by_css_selector("label.s_service_label").text
            print "Spedizione su appuntamento presente"
        finally:
            time.sleep(2)

        try:
            assert u'Spedizione Next Day + 4,90 €' in driver.find_element_by_xpath(
                "//dt[3]/div/span/label").text
            print "Spedizione Next Day presente"
        finally:
            time.sleep(2)

        try:
            assert u'Spedizione Consegna al Piano + 14,90 €' in driver.find_element_by_xpath(
                "//dt[4]/div/span/label").text
            print "Spedizione Consegna al Piano presente"
        finally:
            time.sleep(2)

        try:
            assert "Non voglio servizi di spedizione aggiuntivi" in driver.find_element_by_css_selector(
                "div.label-container > span.d-radio-button > label").text
            print "Servizi aggiuntivi di spedizone presenti"
        finally:
            time.sleep(2)

        try:
            assert "Puoi scegliere in AGGIUNTA uno dei nostri servizi disponibili" in driver.find_element_by_css_selector("div.service_title").text
        finally:
            time.sleep(2)


        try:
            assert "Le spese di spedizione saranno calcolate e mostrate nello step successivo." in driver.find_element_by_css_selector("dd.advise_calculation > label").text
        finally:
            time.sleep(2)



        try:
            assert "Metodi Pagamento" in driver.find_element_by_css_selector("#checkout-step-methods > h4").text
            print "Sezione relativa al pagamento presente"
        finally:
            time.sleep(2)
        try:
            assert "Bonifico" in driver.find_element_by_css_selector(
                "#dt_method_banktransfer > span.d-radio-button > label.label").text
            print "Bonifico presente"
        finally:
            time.sleep(2)
        try:
            assert "PayPal" in  driver.find_element_by_css_selector(
                "#dt_method_paypal_express > span.d-radio-button > label.label").text
            print "Paypal presente"
        finally:
            time.sleep(2)
        try:
            assert ("Carta di Credito"in driver.find_element_by_css_selector(
                "#dt_method_gestpaypro > span.d-radio-button > label.label").text)
            print "Caerta di Credito presente"
        finally:
            time.sleep(2)
        try:
            assert "Contrassegno" in driver.find_element_by_css_selector(
                "#dt_method_msp_cashondelivery > span.d-radio-button > label.label").text
            print "Contrassegno presente"
        finally:
            time.sleep(2)

        metodoPag=driver.find_element_by_css_selector("#dt_method_banktransfer > span.d-radio-button > label.label").text
        driver.find_element_by_css_selector("#dt_method_banktransfer > span.d-radio-button > label.label").click()
        try:
            assert "Bonifico intestato a:\nAlkemy S.p.A., San Gregorio 34 20124 Milano\nCoordinate:\nBANCA SELLA\nIBAN IT28Q0326801605052850880051\nBIC/SWIFT: SELBIT2BXXX\nCausale:nella pagina di conferma ordine e in questa email." in driver.find_element_by_css_selector("div.banktransfer-instructions-content.agreement-content").text
            print "Dati bonifico presenti"
        finally:
            time.sleep(2)

        datiBonifico= driver.find_element_by_css_selector("div.banktransfer-instructions-content.agreement-content").text


        #RIEPILOGO
        try:
            assert "Dati di Fatturazione", driver.find_element_by_css_selector("dt.complete > h4.title").text
            print "Sezione relativa ai dati di fatturazione presente"
        finally:
            time.sleep(2)
        if  nome +' ' + cognome + '\n' + via +'\n' +citta + ', ' +provincia +', ' +cap + '\nItalia\nT: ' +telefono == driver.find_element_by_css_selector("address").text:
            print "I dati di fatturazione sono corretti"
        else:
            print "WARNING: Verifica la correttezza dai dati di fatturazione"

        try:
            assert "DATI DI SPEDIZIONE" in  driver.find_element_by_css_selector("#shipping-progress-opcheckout > dt.complete > h4.title").text
            print "Sezione relativa ai dati di spedizione presente"
        finally:
            time.sleep(2)

        if  nome +' ' + cognome + '\n' + via1 +'\n' +citta2 + ', ' +provincia2+', ' +cap2 + '\nItalia\nT: ' +telefono == driver.find_element_by_css_selector ("#shipping-progress-opcheckout > dd.complete > address").text:
            print "I dati di spedizione sono corretti"
        else:
            print "WARNING: Verifica la correttezza dai dati di spedizione"

        driver.find_element_by_css_selector('#checkout-progress-wrapper > div > div.block-data.progress-accordion.d-accordion > div.block-title.progress-opc-cart > h4').click()

        #VERIFICA TOTALE E SUBTOTALE

        try:
            assert "Subtotale", driver.find_element_by_css_selector("dt.label").text
            print "Il subtotale è presente"
        finally:
            time.sleep(2)

        subtotale2 = driver.find_element_by_css_selector('dd.value > span.price').text

        subCheck2 = subtotale2.replace(',', '.')
        SubC2 = subCheck2[:-2].encode()



        totaleCheck2 = driver.find_element_by_css_selector('dd.value.total > span.price').text
        totCheck2 = totaleCheck2.replace(',', '.')
        totC2 = totCheck2[:-2].encode()

        if tot >= 100:
            if ( round(float(SC), 1) == round(float(SubC2), 1) and
                        round(float(tot), 1) == round(float(totC2), 1)):
                print u"Totale e subtotale corretti"
                print u"L'iva è calcolata correttamente"
            else:
                print u"WARNING: Verifica la correttezza del totale e subtotale"

        else:
            if (round(float(SC), 1) == round(float(SubC2), 1) and
                        float(tot) == float(totC2)+6.5):
                print u"Totale e subtotale corretti"
                print u"L'iva è calcolata correttamente"
            else:
                print u"WARNING: Verifica la correttezza del totale e subtotale"


        try:
           assert "Totale" in  driver.find_element_by_css_selector("dt.label.total").text
           print "Totale presente"
        finally:
            time.sleep(2)

        driver.find_element_by_css_selector(
            "#checkout-step-shipping_payment > div.buttons-set > button.d-button.right").click()

        time.sleep(5)

        #RIEPILOGO ORDINE

        try:
            assert "Riepilogo Ordine" in driver.find_element_by_css_selector("h4._hide-small-mobile").text
            print "Corretto redirect allo Step 3: Rivedi ordine"
        finally:
            time.sleep(2)

        driver.find_element_by_css_selector(
            "div.block-title.progress-opc-billing-shipping.progress-opc-billing-title").click()

        try:
            assert "Dati di Fatturazione", driver.find_element_by_css_selector("dt.complete > h4.title").text
            print "Sezione relativa ai dati di fatturazione presente"
        finally:
            time.sleep(2)

        if  nome +' ' + cognome + '\n' + via +'\n' +citta + ', ' +provincia +', ' +cap + '\nItalia\nT: ' +telefono == driver.find_element_by_css_selector("address").text:
            print "I dati di fatturazione sono corretti"
        else:
            print "WARNING: Verifica la correttezza dai dati di fatturazione"

        try:
            assert "DATI DI SPEDIZIONE" in  driver.find_element_by_css_selector("#shipping-progress-opcheckout > dt.complete > h4.title").text
            print "Sezione relativa ai dati di spedizione presente"
        finally:
            time.sleep(2)

        if  nome +' ' + cognome + '\n' + via1 +'\n' +citta2 + ', ' +provincia2+', ' +cap2 + '\nItalia\nT: ' +telefono == driver.find_element_by_css_selector ("#shipping-progress-opcheckout > dd.complete > address").text:
            print "I dati di spedizione sono corretti"
        else:
            print "WARNING: Verifica la correttezza dai dati di spedizione"

        #driver.find_element_by_css_selector('#checkout-progress-wrapper > div > div.block-data.progress-accordion.d-accordion > div.block-title.progress-opc-cart > h4').click()


        try:
           assert "SPEDIZIONE E PAGAMENTO" in driver.find_element_by_css_selector("#checkout-progress-wrapper > div > div.block-data.progress-accordion.d-accordion > div.block-title.progress-opc-shipping-payment.progress-opc-shipping-title > h4").text
        finally:
           time.sleep(2)

        driver.find_element_by_css_selector(
            '#checkout-progress-wrapper > div > div.block-data.progress-accordion.d-accordion > div.block-title.progress-opc-shipping-payment.progress-opc-shipping-title > h4').click()

        if u"Spedizione Gratuita\n 0,00 €" == driver.find_element_by_css_selector("#shipping_method-progress-opcheckout > dd.complete").text:
            print "Spedizione gratuita"
        else:
            if u"Spedizione Standard\n 6,50 €" == driver.find_element_by_css_selector("#shipping_method-progress-opcheckout > dd.complete").text:
               print "Spedizione standard al costo di 6,50"

        try:
            assert metodoPag in driver.find_element_by_xpath("(.//*[normalize-space(text()) and normalize-space(.)='Spedizione e Pagamento'])[2]/following::p[1]").text
            print u"Il metodo di pagamento è corretto"
        finally:
            time.sleep(2)
        try:
           assert datiBonifico in driver.find_element_by_css_selector("dd.complete > table > tbody > tr > td").text
           print "Dati bonifico verificati correttamente"
        finally:
           time.sleep(2)


        driver.find_element_by_css_selector("div.block-title.progress-opc-cart > h4.title").click()

        if tot >= 100:
            if (round(float(SC), 1) == round(float(SubC2), 1) and
                        round(float(tot), 1) == round(float(totC2), 1)):
                print u"Totale e subtotale corretti"
                print u"L'iva è calcolata correttamente"
            else:
                print u"WARNING: Verifica la correttezza del totale e subtotale"

        else:
            if (round(float(SC), 1) == round(float(SubC2), 1) and
                        float(tot) == float(totC2) + 6.5):
                print u"Totale e subtotale corretti"
                print u"L'iva è calcolata correttamente"
            else:
                print u"WARNING: Verifica la correttezza del totale e subtotale"

        time.sleep(7)


        if tot>= 100:

            subtota=driver.find_element_by_css_selector("td.a-right > span.price").text
            subt3 = subtota.replace(',', '.')
            sub3 = subt3[:-2].encode()

            if round(float(sub3) , 1) == tot:
                print u"Il totale per ordini superiori a 100 euro è corretto"
                print u'La spedizione è gratuita'

        else:
            spedizione2 = driver.find_element_by_xpath("//tr[2]/td[2]/span").text
            sped2 = spedizione2.replace(',', '.')
            S2 = sped2[:-2].encode()
            sum2 = round(float(S2), 1) + float(tot)
            totComplessivo2 = driver.find_element_by_css_selector("strong > span.price").text
            totC2 = totComplessivo2.replace(',', '.')
            TC2 = totC2[:-2].encode()
            if round(float(TC2), 1) == sum2:
                print u"Per ordini inferiori a 100 euro il costo della spedizione è 6,50 euro "
                print u'Il totale complessivo è corretto'

        driver.find_element_by_css_selector("button.d-button.right.btn-checkout").click()
        time.sleep(11)

        #CONCLUSIONE PROCESSO ACQUISTO

        num_ordine = driver.find_element_by_css_selector("div.body > p > a").text

        config.set('SEZIONE_ORDINE', 'numero_ordine', num_ordine)
        numer_ord = config.get('SEZIONE_ORDINE', 'numero_ordine')

        try:
            assert u"Il tuo ordine è stato ricevuto." in driver.find_element_by_css_selector("h1").text
            print u'Il tuo ordine è stato ricevuto  presente'
        finally:
            time.sleep(2)

        try:
            assert u"Ti ringraziamo per il tuo acquisto!" in driver.find_element_by_css_selector("h2.sub-title").text
            print u'Ti ringraziamo per il tuo acquisto! presente'
        finally:
            time.sleep(2)

        driver.find_element_by_css_selector("button.d-button > span.txt").click()




    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()


