#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from pyvirtualdisplay import Display
from selenium import webdriver
from Connection import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, shutil, ConfigParser

reload(sys)
sys.getdefaultencoding()
nomeTest = 'RU_023.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU023_1(unittest.TestCase):
    def test_RubricaIndirizzi(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.get(url + "/")
        print u"Test sulla verifica della Rubrica indirizzi nella sezione my account"

        pathAssoluto = os.getcwd()
        pathFind = pathAssoluto.find('RU_023_Rubrica_Indirizzi')
        pathInit = pathAssoluto[:pathFind]
        src = (pathInit + 'RU_004_Login/ConfigFile.properties')
        dst = (pathAssoluto)
        shutil.copy2(src, dst)

        config = ConfigParser.RawConfigParser()
        config.read('ConfigFile.properties')
        print u"I dati dell'utente sono:"

        nome = config.get('SEZIONE_DATI_UTENTE', 'nome')
        print u"Nome: " +nome
        cognome = config.get('SEZIONE_DATI_UTENTE', 'cognome')
        print u"Cognome: " +cognome
        password = config.get('SEZIONE_PASSWORD', 'password')
        print u"Password: "  +password
        email = config.get('SEZIONE_EMAIL', 'email')
        print u"Email: " +email
        via = config.get('SEZIONE_DATI_UTENTE', 'via2')
        print "Via: " + via




        browser.switch_to.window(browser.window_handles[0])
        browser.find_element_by_css_selector("#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-user.-beopen").click()
        time.sleep(3)
        driver.find_element_by_id("email_menu").clear()
        driver.find_element_by_id("email_menu").send_keys(email)
        driver.find_element_by_id("pass_menu").clear()
        driver.find_element_by_id("pass_menu").send_keys("Utente001")

        driver.find_element_by_css_selector("#send2").click()
        time.sleep(5)

        titolo=driver.title
        if titolo== 'Il mio account':
            print u"Accesso all'account utente effettuato con successo"
        else:
            print u"WARNING: Non è stato possibile effettuare l'accesso all'account utente"

        driver.find_element_by_xpath("(//a[contains(text(),'Rubrica')])[2]").click()
        time.sleep(7)
        driver.find_element_by_css_selector("button.d-button > span.txt").click()
        time.sleep(2)
        if (driver.find_element_by_id("advice-required-entry-tax_code").text==u"Questo è un campo obbligatorio." and
           driver.find_element_by_id("advice-required-entry-telephone").text == u"Questo è un campo obbligatorio." and
           driver.find_element_by_id("advice-required-entry-city").text == u"Questo è un campo obbligatorio." and
           driver.find_element_by_id("advice-required-entry-zip").text == u"Questo è un campo obbligatorio." and
           driver.find_element_by_id("advice-required-entry-street_1").text == u"Questo è un campo obbligatorio."):
           print u"Verificata la corretta gestione dei messaggi di warning sui campi obbligatori"
        else:
            print u"WARNING: I messaggi di warning sui campi obbligatori non sono tutti presenti"


        if (driver.find_element_by_id("firstname").get_attribute("value")== nome and
            driver.find_element_by_id("lastname").get_attribute("value") == cognome):
            print u"I campi nome e cognome sono correttamente precompilati"
        else:
            print u"WARNING: I campi nome e cognome non sono correttamente precompilati"


        if driver.find_element_by_xpath("//form[@id='form-validate']/div/div[7]/div/div/span/span/span").text == 'Italia':
            print u"Il paese è correttamente impostato"

        driver.find_element_by_id('tax_code').send_keys('ggggg')
        driver.find_element_by_css_selector("button.d-button > span.txt").click()
        time.sleep(2)
        if driver.find_element_by_id('advice-validate-tax_code-tax_code').text=='Inserisci un codice fiscale valido.':
            print u"Verifica sulla validità del codice fiscale effettuata con successo"

        config.set('SEZIONE_DATI_UTENTE', 'codice', 'UTDHLR88P56C987O')
        codiceFiscale = config.get('SEZIONE_DATI_UTENTE', 'codice')


        config.set('SEZIONE_DATI_UTENTE', 'telefono', '12334')
        telefono= config.get('SEZIONE_DATI_UTENTE', 'telefono')


        config.set('SEZIONE_DATI_UTENTE', 'zip' , '78965')
        zip = config.get('SEZIONE_DATI_UTENTE', 'zip')


        with open('ConfigFile.properties', 'w') as configfile:
            config.write(configfile)

        driver.find_element_by_id('tax_code').clear()
        driver.find_element_by_id('tax_code').send_keys(codiceFiscale)
        driver.find_element_by_id('telephone').send_keys(telefono)
        driver.find_element_by_css_selector('#form-validate > div:nth-child(1) > div:nth-child(9) > div > div > span > span').click()
        driver.find_element_by_css_selector('#form-validate > div:nth-child(1) > div:nth-child(9) > div > div > ul > li:nth-child(18)').click()
        provincia=driver.find_element_by_css_selector('#form-validate > div:nth-child(1) > div:nth-child(9) > div > div > ul > li:nth-child(18)').get_attribute('innerHTML')
        driver.find_element_by_css_selector('#form-validate > div:nth-child(1) > div:nth-child(10) > div > div.d-select.-slim > span').click()
        time.sleep(3)
        driver.find_element_by_xpath('//*[@id="form-validate"]/div[1]/div[9]/div/div[1]/ul/li[3]').click()
        citta=driver.find_element_by_xpath('//*[@id="form-validate"]/div[1]/div[9]/div/div[1]/ul/li[3]').get_attribute('innerHTML')

        config.set('SEZIONE_DATI_UTENTE', 'provincia', provincia)
        config.get('SEZIONE_DATI_UTENTE', 'provincia')

        config.set('SEZIONE_DATI_UTENTE', 'citta', citta)
        config.get('SEZIONE_DATI_UTENTE', 'citta')

        with open('ConfigFile.properties', 'w') as configfile:
            config.write(configfile)

        driver.find_element_by_id('zip').send_keys(zip)
        driver.find_element_by_id('street_1').send_keys(via)

        driver.find_element_by_css_selector('button.d-button.validation-passed > span.txt').click()

        time.sleep(4)
        # if (driver.find_element_by_css_selector('div.d-msg').text=='Success!' and
        #    driver.find_element_by_css_selector('div.d-global-messages > ul > li > ul > li > span').text== "Indirizzo salvato."):
        #     print "Indirizzo salvato con successo"

        #driver.find_element_by_css_selector('path.d_close').click()

        if (driver.find_element_by_css_selector('address').text== nome +' ' + cognome + '\n' + via +'\n' +citta + ', ' +provincia +', ' +zip + '\nItalia\nT: ' +telefono and
            driver.find_element_by_css_selector('div.col-2 > div.box > div.box-content > address').text == nome + ' ' + cognome + '\n' + via + '\n' + citta + ', ' + provincia + ', ' + zip + '\nItalia\nT: ' + telefono):
            print u" I dati dell' Indirizzo di fatturazione e spedizione sono corretti "
        else:
            print  u"WARNING: I dati dell' Indirizzo di fatturazione e spedizione non sono corretti"

        driver.find_element_by_css_selector('div > div.box-content > a').click()

        time.sleep(4)


        via2= config.get('SEZIONE_DATI_UTENTE', 'via1')


        driver.find_element_by_id('street_1').clear()
        driver.find_element_by_id('street_1').send_keys(via2)

        driver.find_element_by_css_selector('#form-validate > div.buttons-set > button').click()

        # if (driver.find_element_by_css_selector('div.d-msg').text=='Success!' and
        #    driver.find_element_by_css_selector('div.d-global-messages > ul > li > ul > li > span').text== "Indirizzo salvato."):
        #     print "Indirizzo salvato con successo"
        #
        # driver.find_element_by_css_selector('path.d_close').click()

        if (driver.find_element_by_css_selector('address').text== nome +' ' + cognome + '\n' + via2+'\n' +citta + ', ' +provincia +', ' +zip + '\nItalia\nT: ' +telefono and
            driver.find_element_by_css_selector('div.col-2 > div.box > div.box-content > address').text == nome + ' ' + cognome + '\n' + via2 + '\n' + citta + ', ' + provincia + ', ' + zip + '\nItalia\nT: ' + telefono):
            print u" I dati dell' Indirizzo di fatturazione e spedizione sono corretti "
        else:
            print  u"WARNING: I dati dell' Indirizzo di fatturazione e spedizione non sono corretti"

        browser.find_element_by_css_selector("#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-user.-beopen").click()

        driver.switch_to_active_element()
        driver.find_element_by_xpath('//*[@id="d-header-main"]/div/section/section[3]/div[7]/div/div/div/div/div/div[1]/a[2]').click()

        time.sleep(7)

        driver.find_element_by_css_selector('path.d_close').click()



        browser.find_element_by_css_selector("#d-header-main > div > section > section.top-wrap > div.wrap > div.dx > ul > li.link.top-user.-beopen").click()
        time.sleep(3)
        driver.find_element_by_id("email_menu").clear()
        driver.find_element_by_id("email_menu").send_keys(email)
        driver.find_element_by_id("pass_menu").clear()
        driver.find_element_by_id("pass_menu").send_keys(password)

        driver.find_element_by_css_selector("#send2").click()
        time.sleep(5)

        driver.find_element_by_xpath("(//a[contains(text(),'Rubrica')])[2]").click()
        time.sleep(7)

        if (driver.find_element_by_css_selector('address').text== nome +' ' + cognome + '\n' + via2+'\n' +citta + ', ' +provincia +', ' +zip + '\nItalia\nT: ' +telefono and
            driver.find_element_by_css_selector('div.col-2 > div.box > div.box-content > address').text == nome + ' ' + cognome + '\n' + via2 + '\n' + citta + ', ' + provincia + ', ' + zip + '\nItalia\nT: ' + telefono):
            print u" I dati dell' Indirizzo di fatturazione e spedizione sono corretti "
        else:
            print  u"WARNING: I dati dell' Indirizzo di fatturazione e spedizione non sono corretti"



    def is_element_present(self, how, what):
        try:
            browser.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            browser.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = browser.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        browser.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest()