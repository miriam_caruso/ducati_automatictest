#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from selenium import webdriver
from Connection import*
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, sys, os, ConfigParser, shutil
reload(sys)
sys.getdefaultencoding()

nomeTest = 'RU_009_2.py'
print('START '+nomeTest)

# SET for Bamboo
# options = webdriver.ChromeOptions()
# options.add_argument("--headless")
# options.add_argument("--disable-web-security")
# options.add_argument('--no-sandbox')
# options.add_argument("--disable-setuid-sandbox")
# options.add_argument("--window-size=1920,1080")
# options.add_argument("--disable-dev-shm-usage")
# driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper

class RU009_2(unittest.TestCase):
    def test_Checkout_not_logged(self):
        self.verificationErrors = []
        self.accept_next_alert = True
        self.maxDiff = None
        driver = browser
        driver.implicitly_wait(100)
        driver.get(url + "/")

        driver.find_element_by_css_selector("div.hamburger").click()
        time.sleep(4)
        print 'click sul tasto hamburger'
        driver.find_element_by_css_selector("a[title=\"Felpe & Maglioni\"]").click()
        time.sleep(3)
        print 'click sulla categoria felpe & maglioni'
        element_to_hover_over = driver.find_element_by_xpath("//div[2]/div/div/div[2]/div/div/div/div")

        hover = ActionChains(browser).move_to_element(element_to_hover_over)
        hover.perform()
        driver.switch_to_active_element()
        print 'mousehover sulla sezione shop now eseguita'
        driver.find_element_by_css_selector("div.d-card-imgtext.-products").click()
        time.sleep(3)
        print 'redirect sul dettaglio prodotto'
        driver.find_element_by_css_selector("#addtocartbtn > span.txt").click() #button aggiungi al carrello

        print 'aggiunta al carrello del primo prodotto andato a buon fine'
        time.sleep(3)


        driver.find_element_by_xpath(
            "//div[@id='d-header-main']/div/section/section/div[2]/div[2]/ul/li[4]/a/span/span[2]").click()   #apertura pannello minicart

        driver.switch_to_active_element()
        time.sleep(3)
        prodotto1= driver.find_element_by_css_selector("div.product-name > a > span").text

        time.sleep(3)
        quantita1= driver.find_element_by_css_selector("span.qty").text

        time.sleep(3)
        prezzo1 =driver.find_element_by_css_selector("span.price").text

        price1= prezzo1[:-2]

        pr1= float(price1.replace(',', '.').encode())

        #p1=float(pr1)

        print pr1

        #driver.get(url + "/it/it/checkout/cart/")
        #time.sleep(3)
         #driver.find_element_by_css_selector("button.d-button.btn-proceed-checkout.btn-checkout > span.txt").click()
        driver.find_element_by_css_selector("a.d-button.btn-proceed-checkout.btn-checkout > span.txt").click()
        try:
            assert "Accedi" in driver.find_element_by_css_selector("div.std > h4").text
            print 'il titolo Accedi è presente'
        finally:
            time.sleep(2)
        try:
            assert u"Inserisci i tuoi dati di accesso per poter procedere con l’acquisto." in driver.find_element_by_css_selector(
                "p").text
            print 'il testo inserisci i tuoi dati..  è presente'
        finally:
            time.sleep(2)

        try:
            assert "Hai dimenticato la password?" in driver.find_element_by_css_selector("a.f-pwd").text
            print 'il titolo Hai dimenticato la password? è presente'
        finally:
            time.sleep(2)
        try:
            assert "ACCEDI" in driver.find_element_by_css_selector("button.d-button.-gray").text
            print 'il bottone ACCEDI è presente'
        finally:
            time.sleep(2)
            # try:
            #   assert u"ACCEDI CON FACEBOOK" in driver.find_element_by_xpath("//span[2]").text
            #  print 'Accedi con FB è presente'
            # finally:
            #    time.sleep(2)
            #        print 'la sezione Accedi è presente'

        try:
            assert 'Nuovo utente' in driver.find_element_by_css_selector("div.block-checkout-new>div.std>h4").text
            print  'Nuovo utente è presente'
        finally:
            time.sleep(2)
        try:
            assert "Creando un account con il nostro negozio sarai in grado di muoverti velocemente verso gli ordini, effettuare ordini con indirizzi di spedizione multipli, visualizzare e tracciare i tuoi ordini nel tuo account" in driver.find_element_by_css_selector(
                "div.block-checkout-new > div.std > p").text
            print 'il testo creando... è presente'
        finally:
            time.sleep(2)
        try:
            assert "REGISTRATI" in driver.find_element_by_css_selector("#onepage-guest-register-button > span.txt").text
            print 'il bottone Registrati è presente'
        finally:
            time.sleep(2)
        try:
            assert "Continua come ospite" in driver.find_element_by_css_selector(
                "div.block-checkout-login_guest > div.std > h4").text
            print 'Continua come ospite è presente'
        finally:
            time.sleep(2)
        try:
            assert u"Inserisci i tuoi dati di accesso per poter procedere con l’acquisto." in driver.find_element_by_css_selector(
                "div.block-checkout-login_guest > div.std > p").text
            print 'Inserisci i tuoi dati.. è presente'
        finally:
            time.sleep(2)
        try:
            assert "CONTINUA COME OSPITE" in driver.find_element_by_css_selector("#onepage-guest-guest-button").text
            print  " il bottone Continue as a guest è presente"
        finally:
            time.sleep(2)
        driver.find_element_by_css_selector("#onepage-guest-guest-button > span.txt").click()    #click accedi come ospite
        print 'click accesso come ospite andato a buon fine e redirect avvenuto con successo'
        time.sleep(2)













